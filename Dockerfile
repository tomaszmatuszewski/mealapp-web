# specify the node base image with your desired version node:<version>
FROM node:12-stretch
# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH
RUN apt-get update -y && apt-get upgrade -y

# install app dependencies
COPY package.json ./
COPY yarn.lock ./
RUN yarn install --silent --network-timeout 1000000000
RUN yarn add react-scripts@3.4.1 --silent

# add app
COPY . ./

