### W celu uruchomienia projektu potrzeba:

1. `Node.js (wersja 12.16.1)`
Do pobrania [tutaj](https://nodejs.org/en/download/)

2. Wraz z instalacją Node.js oprogramowanie Chocolatey przy instalacji pobierze inne potrzebne narzędzia.

3. Stąd pobieramy yarna: [tutaj](https://classic.yarnpkg.com/en/docs/install/#windows-stable). Jest w wersji `1.22.4`.

4. Sprawdzamy teraz, czy wszystko zainstalowało się pomyślnie. Odpalamy wiersz poleceń i wywołujemy komendę `node --version`

5. Kolejną komendą do sprawdzenia jest `yarn --version`

6. Teraz możemy pobrać repozytorium na nasz komputer.

7. Ponownie uruchamiamy wiersz poleceń i przechodzimy do katalogu gdzie znajduje się pobrany projekt `cd (...)`

8. Będąc w katalogu projektu uruchamiamy komendę `yarn install`, aby zainstalować wszystkie pakiety z pliku `package.json`, które są niezbędnymi dla działania projektu. Chociaż najprawdopodobniej i tak sami zostaniemy zapytani o ich instalację po otwarciu katalogu projektowego - tak więc - zgadzamy się.

9. Uruchamiamy serwer testowy komendą `yarn start`. Powinniśmy zobaczyć poprawnie renderującą się stronę.

Przygotował: Tomek
