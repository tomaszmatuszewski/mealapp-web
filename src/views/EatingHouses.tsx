import React, { useState, useEffect } from 'react';
import Navigation from '../components/menu/Navigation';
import { makeStyles, Theme } from '@material-ui/core/styles';
import EatingHousesService, { IEatingHousesProps } from '../services/EatingHousesService';
import ViewTitle from '../components/viewTitle/ViewTitle';
import EatingHouse from '../components/eatingHouse/EatingHouse';
import CustomLoading from '../components/customLoading/CustomLoading';
import emptyEatingHousesIcon from '../img/emptyEatingHouses.png'
import { useParams } from 'react-router';
import backgroundImg from '../img/eatingHousesImg.jpg';
import { TextField } from '@material-ui/core';
import BlueButton from '../components/buttons/BlueButton';
import {Autocomplete} from "@material-ui/lab";
//import Autocomplete from '@material-ui/lab/Autocomplete';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: 'rgba(238, 238, 238, 0.81)',
        backgroundImage: `url(${backgroundImg})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundBlendMode: 'color',
        minHeight: '100vh',
        minWidth: '100%',
        paddingBottom: '3%',
    },
    mainContainer: {
        margin: '15px',
    },
    dataContainer: {
        margin: 'auto auto auto auto',
        maxWidth: '95%',
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
    },
    itemContainer: {
        marginBottom: '-15px',
        margin: '1%',
        width: '600px',
    },
    emptyMainContainer: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        margin: 'auto',
        alignItems: 'center',
    },
    emptyListContainer: {
        width: '280px',
        marginTop: theme.spacing(4),
    },
    icon: {
        width: '150px',
        height: '150px',
    },
    infoContainer: {
        border: '2px solid #f3969a',
        width: '280px',
        borderRadius: '12px',
        backgroundColor: '#ffffff',
    },
    searchContainer: {
        marginLeft: 'auto',
        marginRight: 'auto',
        display: 'flex',
        flexDirection: 'column',
        minWidth: '300px',
        maxWidth: '600px',
        marginTop: '2%',
        marginBottom: '2%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    searchField: {
        "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
            borderWidth: '2px',
            borderColor: "#f3969a",
        },
        maxWidth: '80%',
        margin: 'auto',
        marginTop: '1%',
        marginBottom: '1%',
        backgroundColor: 'white',
    },
    buttonContainer: {
        margin: 'auto',
        paddingRight: '1%',
    },
    backButton: {
        marginTop: '2%',
    }
}));

const EatingHouses = () => {
    const classes = useStyles();
    const [eatingHouses, setEatingHouses] = useState<IEatingHousesProps | null>(null);
    const [eatingHousesCopy, setEatingHousesCopy] = useState<IEatingHousesProps | null>(null);
    const [isLoading, setIsLoading] = useState(true);
    const { voivodeshipName, cityName } = useParams();
    const [searchText, setSearchText] = useState<string | null>(null);
    const [noSearchResults, setNoSearchResult] = useState(false);

    useEffect(() => {
        EatingHousesService.getEatingHouses(voivodeshipName, cityName).then(resp => {
            if (resp) {
                setEatingHouses(resp);
                setEatingHousesCopy(resp);
            }
            setIsLoading(false);
        });
    }, [voivodeshipName, cityName, isLoading])

    const filterByName = () => {
        let resultArr: IEatingHousesProps = { eatingHouses: [] };
        if (searchText !== null) {
            setEatingHousesCopy(eatingHouses);
            eatingHouses?.eatingHouses.forEach(eatingHouse => {
                if (eatingHouse.name === searchText) {
                    resultArr.eatingHouses.push(eatingHouse);
                    setNoSearchResult(false);
                }
            })
        }

        if (eatingHouses !== null) {
            setEatingHouses(resultArr);
            setNoSearchResult(false);
        }
    }

    useEffect(() => {
        if (searchText === '') {
            setEatingHouses(eatingHousesCopy);
        }
        if (eatingHouses === null) {
            setNoSearchResult(true);
        }

    }, [searchText, eatingHouses, eatingHousesCopy])

    const handlePageRefresh = () => {
        window.location.reload();
    }

    return (
        <div className={classes.root}>
            <Navigation menuIcon={true} />
            <div className={classes.mainContainer}>
                {isLoading ? <CustomLoading /> :
                    <div>
                        {(eatingHouses !== null && eatingHouses.eatingHouses.length === 0) ?
                            <div className={classes.emptyMainContainer}>
                                <div className={classes.emptyListContainer}>
                                    <img src={emptyEatingHousesIcon} className={classes.icon} alt="Brak powiadomień" />
                                </div>
                                <div className={classes.infoContainer}><ViewTitle title={`Ups, nie ma\ntakich restauracji`} /></div>
                                {!noSearchResults ? <div className={classes.backButton}><BlueButton text={"Powróć"} handleOnClick={handlePageRefresh} /></div> : null}
                            </div>
                            :
                            <div>
                                <ViewTitle title={`Sprawdź menu pobliskich jadłodajni i zaspokój głód`} />
                                {eatingHouses ?
                                    <Autocomplete
                                        onInputChange={(event, newInputValue) => {
                                            setSearchText(newInputValue);
                                        }}

                                        freeSolo
                                        options={eatingHouses?.eatingHouses.map((option) => option.name)}
                                        renderInput={(params) => (
                                            <div className={classes.searchContainer}>
                                                <TextField {...params}
                                                    label="Wyszukaj jadłodajnie..."
                                                    margin="normal"
                                                    variant="outlined"
                                                    className={classes.searchField}
                                                    InputLabelProps={{ style: { color: '#6495ed' } }} />
                                                <div className={classes.buttonContainer}>
                                                    <BlueButton text="Wyszukaj" handleOnClick={filterByName} />
                                                </div>
                                            </div>
                                        )}
                                    />
                                    :
                                    null
                                }
                                <div className={classes.dataContainer}>
                                    {!!eatingHouses?.eatingHouses.length && eatingHouses?.eatingHouses.map(eatingHouse => (
                                        <div className={classes.itemContainer} key={eatingHouse.id}>
                                            <EatingHouse
                                                id={eatingHouse.id}
                                                name={eatingHouse.name}
                                                menuList={eatingHouse.menuList}
                                                logoUrl={eatingHouse.logoUrl}
                                                description={eatingHouse.description}
                                                primaryColor={eatingHouse.primaryColor}
                                                slug={eatingHouse.slug}
                                                voivodeship={voivodeshipName}
                                                city={cityName}
                                                deleteIcon={false}
                                            />
                                        </div>
                                    ))}
                                </div>
                            </div>
                        }
                    </div>
                }
            </div>
        </div>
    );
}

export default EatingHouses;