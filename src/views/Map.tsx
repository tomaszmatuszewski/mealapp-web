import React, {useCallback, useEffect, useState} from 'react';
import Navigation from '../components/menu/Navigation';
import {makeStyles, Theme} from '@material-ui/core/styles';
import { GoogleMap, InfoWindow, Marker, LoadScript } from '@react-google-maps/api'
import ViewTitle from "../components/viewTitle/ViewTitle";
import CustomLoading from "../components/customLoading/CustomLoading";
import MapService, {IAddressProps, IMapMarkerProps, IMapMarkersProps} from '../services/MapService'
import BlueButton from "../components/buttons/BlueButton";
import { NavLink } from "react-router-dom";
import Connection from "../services/Connection"
import {Typography} from '@material-ui/core';
import {useSelector} from "react-redux";
import {locationSelector} from "../store/selectors/location.selectors";
import {ILocationStoreState} from "../store/reducers/location.reducer";
import SelectInput from "../components/inputs/SelectInput";
import LocationService, {
    ICitiesProps,
    ICityProps,
    IVoivodeshipProps,
    IVoivodeshipsProps
} from "../services/LocationService";
import locationLogo from "../img/place.png";
import backgroundImg from '../img/mapImg.jpg';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: 'rgba(238, 238, 238, 0.81)',
        backgroundImage: `url(${backgroundImg})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundBlendMode: 'color',
        minHeight: '100vh',
        minWidth: '100%',
        paddingBottom: '3%',
    },
    mainContainer: {
        margin: '15px',
    },
    dataContainer: {
        margin: '15px auto auto auto',
        maxWidth: '95%',
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
    },
    map: {
        width: '60vmax',
        height: '70vmin',
        border: '2px solid #f3969a',
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        margin: 'auto',
        alignItems: 'center',
    },
    infoWindow: {
        textAlign: 'center',
    },
    navlink: {
        textDecoration: 'none',
    },
    flexContainer: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        margin: 'auto',
        alignItems: 'center',
    },
    flexItem: {
        width: '280px',
        marginTop: theme.spacing(4),
    },
    logo: {
        width: '150px',
        height: '150px',
        marginTop: '15px',
    },
    topMargin: {
        marginTop: theme.spacing(2),
    },
}));

const mapControls = {
    mapTypeControl: false,
    streetViewControl: false,
    fullscreenControl: false,
    clickableIcons: false,
};

// function renderMarker(point : IMapMarkerProps){
//     const contentText = 'Kliknij by przejść\ndo jadłodajni';
//     return(
//         <Marker
//             position={{
//             lat: point.lat,
//             lng: point.lng
//             }}
//             key={point.id}
//             onClick={() =>(
//                 <InfoWindow
//                     position={{lat: point.lat, lng: point.lng}}
//                 >
//                     <div>{point.title}</div>
//                     <div>{point.detail}</div>
//                 </InfoWindow>
//             )}
//         >
//         </Marker>
//     )
// }

const Map = () => {
    const classes = useStyles();
    const location = useSelector(locationSelector);
    const [mapMarkers, setMapMarkers] = useState<IMapMarkersProps | null>(null);
    const [selectedPoint, setSelectedPoint] = useState<IMapMarkerProps | null>(null);
    const [selectedMarker, setSelectedMarker] = useState<IAddressProps | null>(null);
    const [mapLocation, setMapLocation] = useState<ILocationStoreState>(location);
    // const [mapCenter, setMapCenter] = useState({ lat: 53.77020960646819, lng: 20.4703061185026 });
    const [mapCenter, setMapCenter] = useState({ lat: mapLocation.latitude, lng: mapLocation.longitude });
    const [voivodeships, setVoivodeships] = useState<IVoivodeshipsProps>();
    const [cities, setCities] = useState<ICitiesProps>();
    const [disabled, setDisabled] = useState(true);
    const [voivodeshipValue, setVoivodeshipValue] = useState<IVoivodeshipProps>({slug: mapLocation.voivodeshipSlug, name: mapLocation.voivodeshipName });
    const [cityValue, setCityValue] = useState<ICityProps>({
        id: mapLocation.cityId,
        slug: mapLocation.citySlug,
        name: mapLocation.cityName,
        zoomWeb: mapLocation.zoom,
        longitude: mapLocation.longitude,
        latitude: mapLocation.latitude
    });

    const handleSelectVoivodeship = (event: any) => {
        setVoivodeshipValue(event.target.value);
        setCityValue({
            id: -1,
            slug: 'none',
            name: 'none',
            zoomWeb: -1,
            longitude: -1,
            latitude: -1
        });
    };

    const handleSelectCity = (event: any) => {
        setCityValue(event.target.value)
    };

    const handleButtonMap = () => {
        setMapLocation({
            voivodeshipSlug: voivodeshipValue.slug,
            voivodeshipName: voivodeshipValue.name,
            citySlug: cityValue.slug,
            cityName: cityValue.name,
            zoom: cityValue.zoomWeb,
            longitude: cityValue.longitude,
            latitude: cityValue.latitude,
            cityId: cityValue.id,
        });
        setMapCenter({ lat: cityValue.latitude, lng: cityValue.longitude });
    };

    const formValidation = useCallback(() => {
        return cityValue.name === 'none';

    }, [cityValue]);

    useEffect(() => {
        MapService.getMapMarkers(mapLocation.voivodeshipSlug, mapLocation.citySlug).then(resp => {
            if (resp) {
                setMapMarkers(resp);
            }
        });
        console.log(mapLocation);
    }, [mapLocation]);

    useEffect(() => {
        LocationService.getVoivodeships().then(resp => {
            if (resp) {
                setVoivodeships(resp);
            }
        });
    }, []);

    useEffect(() => {
        LocationService.getCities(voivodeshipValue.slug).then(resp => {
            if (resp) {
                setCities(resp);
            }
        });
    }, [voivodeshipValue]);

    useEffect(() => {
        setDisabled(formValidation());
    }, [formValidation]);

    return (
        <div className={classes.root}>
            <Navigation menuIcon={true} />
            <div className={classes.mainContainer}>
                <ViewTitle title={`Mapa jadłodajni`} />
                <div className={classes.dataContainer}>
                    <LoadScript
                        googleMapsApiKey={Connection.getMap()}
                        loadingElement={<CustomLoading />}
                    >
                        <GoogleMap
                            zoom={mapLocation.zoom}
                            center={mapCenter}
                            //center={{lat: location.latitude, lng: location.longitude}}
                            mapContainerClassName={classes.map}
                            options={mapControls}
                        >
                            {/*{!!mapMarkers?.mapMarkers.length && mapMarkers?.mapMarkers.map(point => renderMarker(point))}*/}
                            {!!mapMarkers?.mapMarkers.length && mapMarkers?.mapMarkers.map(point => (
                                point.addressList.map(marker => (
                                    <Marker
                                        key = {marker.id}
                                        position={{
                                            lat: marker.latitude,
                                            lng: marker.longitude
                                        }}
                                        onClick={() => {
                                            setSelectedMarker(marker);
                                            setSelectedPoint(point);
                                            setMapCenter({ lat: marker.latitude, lng: marker.longitude })
                                        }}
                                        title = {`${point.name}`}
                                    />
                                ))
                            ))}
                            {selectedMarker && (
                                <InfoWindow
                                    position={{ lat: selectedMarker.latitude, lng: selectedMarker.longitude }}
                                    onCloseClick={() => setSelectedMarker(null)}
                                >
                                    <div className={classes.infoWindow}>
                                        <h3>{selectedPoint?.name}</h3>
                                        <h4>{selectedMarker.address}</h4>
                                        <NavLink to={`/eatingHouses/${selectedPoint?.slug}/voivodeship/${mapLocation.voivodeshipSlug}/place/${mapLocation.citySlug}`} className={classes.navlink}>
                                            <BlueButton buttonSize='small' buttonType='button' text='Przejdź' />
                                        </NavLink>
                                    </div>
                                </InfoWindow>
                            )}
                        </GoogleMap>
                    </LoadScript>
                    <form className={classes.flexContainer}>
                        <Typography variant="h6" className={classes.topMargin}>Zmiana lokalizacji</Typography>
                        <img className={classes.logo} src={locationLogo} alt="location logo" />
                        <div className={classes.flexItem}>
                            <SelectInput
                                labelId='voivodeshipLabel'
                                labelText='Województwo'
                                selectValue={voivodeshipValue.slug}
                                inputOnChange={handleSelectVoivodeship}
                                defaultValue={voivodeshipValue.name === location.voivodeshipName && voivodeshipValue.name !== 'none' ? location.voivodeshipName : 'Wybierz województwo...'}
                                items={voivodeships?.voivodeships}
                            />
                        </div>
                        <div className={classes.flexItem}>
                            <SelectInput
                                labelId='cityLabel'
                                labelText='Miasto'
                                selectValue={cityValue.slug}
                                inputOnChange={handleSelectCity}
                                defaultValue={cityValue.name === location.cityName && cityValue.name !== 'none' ? mapLocation.cityName : 'Wybierz miasto...'}
                                items={cities?.cities}
                            />
                        </div>
                        <div className={classes.flexItem}>
                            <BlueButton disabled={disabled} text="Pokaż" handleOnClick={handleButtonMap} type='submit'/>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    );
};

export default Map;