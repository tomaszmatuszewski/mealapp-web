import React, { useState, useEffect, useRef } from 'react';
import Navigation from '../components/menu/Navigation';
import { makeStyles, Theme } from '@material-ui/core/styles';
import BlueButton from '../components/buttons/BlueButton';
import { Dialog, DialogTitle, DialogContent, DialogContentText, TextField, DialogActions } from '@material-ui/core';
import Notification from '../components/notification/Notification';
import notificationsIcon from '../img/emptyNotifications.png';
import ViewTitle from '../components/viewTitle/ViewTitle';
import backgroundImg from '../img/notificationsImg.jpg';
import { NotificationsService } from '../services/NotificationsService';
import { INotification } from '../store/reducers/notifications.reducer';
import { notificationsSelector } from '../store/selectors/notifications.selectors';
import { useSelector } from 'react-redux';
import { useService } from '../hooks/useService';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: 'rgba(238, 238, 238, 0.81)',
        backgroundImage: `url(${backgroundImg})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundBlendMode: 'color',
        minHeight: '100vh',
        minWidth: '100%',
        paddingBottom: '3%',
    },
    mainContainer: {
        margin: '15px',
        textAlign: 'center',
    },
    flexContainer: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        margin: 'auto',
        alignItems: 'center',
    },
    flexItem: {
        width: '300px',
        marginTop: theme.spacing(4),
    },
    notificationsContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        maxWidth: '95%',
        margin: 'auto',
        justifyContent: 'center',
    },
    notificationItem: {
        width: '300px',
        margin: theme.spacing(2),
        backgroundColor: '#ffffff',
    },
    dialog: {
        border: '2px solid #f3969a',
        borderRadius: '12px',
    },
    infoContainer: {
        border: '2px solid #f3969a',
        width: '300px',
        borderRadius: '12px',
        backgroundColor: '#ffffff',
    },
    icon: {
        width: '200px',
        height: '200px',
    },
    error: {
        fontSize: '0.8em',
        color: 'red',
    }
}));

const Notifications = () => {
    const classes = useStyles();
    const [openDialog, setOpenDialog] = useState(false);
    const [okButtonDisabled, setOkButtonDisabled] = useState(true);
    const [addButtonDisabled, setAddButtonDisabled] = useState(false);
    const [notification, setNotification] = useState<INotification>({ id: 0, label: '', isOn: true });
    const notifications = useSelector(notificationsSelector);
    const notificationsService = useService(NotificationsService);
    const [errorAddNotification, setErrorAddNotification] = useState('');
    const [errorToMuchNotifications, setErrorToMuchNotifications] = useState('');
    const firstRender = useRef(true);

    /* Dla panowania nad ilością powiadomień */
    useEffect(() => {
        if (notifications.notifications.length >= 10) {
            setAddButtonDisabled(true);
            setErrorToMuchNotifications('Nie można dodać więcej powiadomień');
        }
        else if (notifications.notifications.length < 10) {
            setAddButtonDisabled(false);
            setErrorToMuchNotifications('');
        }
    }, [notifications.notifications.length])

    /* Dla panowania nad dostępnością przycisku Ok w Dialogu */
    useEffect(() => {
        if (firstRender.current) {
            firstRender.current = false;
            return
        }
        else if (notification?.label !== '' && notification.label.length <= 25) {
            setOkButtonDisabled(false);
            setErrorAddNotification('');
        }
        else {
            setOkButtonDisabled(true);
            setErrorAddNotification('Powiadomienie nie może być puste i musi mieć maksymalnie 25 znaków');
        }
    }, [notification])

    const handleClickOpen = () => {
        setOpenDialog(true);
        let index = 0;
        if (notifications.notifications.length !== 0)
            index = notifications.notifications[notifications.notifications.length-1].id + 1;
        setNotification(prevState => {
            return {...prevState, id: index}
        })
        setOkButtonDisabled(true);
    };

    const handleTextInput = (event: any) => {
        const val = event.target.value;
        setNotification(prevState => {
            return { ...prevState, label: val }
        })
    };

    const handleCheckSwitcher = (id: number) => {
        notificationsService.setNotificationOn(id);
    }

    const handleAddNotification = () => {
        setOpenDialog(false);
        setOkButtonDisabled(true);
        setNotification(prevState => {
            return { ...prevState, isOn: true }
        })
        notificationsService.addNotification(notification);
    }

    const handleDeleteNotification = (id: number) => {
        notificationsService.deleteNotification(id);
    }

    return (
        <div className={classes.root}>
            <Navigation menuIcon={true} />
            <div className={classes.mainContainer}>
                <Dialog fullWidth maxWidth={'sm'} classes={{ paper: classes.dialog }} open={openDialog} onClose={() => setOpenDialog(false)} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Dodaj powiadomienie</DialogTitle>
                    <form>
                        <DialogContent>
                            <DialogContentText>Wpisz nazwę alertu:</DialogContentText>
                            <TextField fullWidth autoFocus label="Alert" variant="outlined" onChange={handleTextInput}></TextField>
                        </DialogContent>
                        <DialogActions>
                            <h1 className={classes.error}>{errorAddNotification}</h1>
                            <BlueButton text="Anuluj" buttonType="button" handleOnClick={() => setOpenDialog(false)} />
                            <BlueButton text="Ok" buttonType="button" handleOnClick={handleAddNotification} disabled={okButtonDisabled} />
                        </DialogActions>
                    </form>
                </Dialog>
                <div className={classes.flexContainer}>
                    {(notifications.notifications.length) === 0 ?
                        <div>
                            <div className={classes.flexItem}>
                                <img src={notificationsIcon} className={classes.icon} alt="Brak powiadomień" />
                            </div>
                            <div className={classes.infoContainer}><ViewTitle title={`Coś tutaj pusto...\ndodaj powiadomienie`} /></div>
                        </div>
                        :
                        <div className={classes.notificationsContainer}>
                            {notifications.notifications.map((notification) => (
                                <div className={classes.notificationItem} key={notification.id}>
                                    <Notification
                                        id={notification.id}
                                        label={notification.label}
                                        isOn={notification.isOn}
                                        checkHandler={handleCheckSwitcher}
                                        deleteHandler={handleDeleteNotification}
                                        />
                                </div>

                            ))}
                        </div>
                    }
                    <div className={classes.flexItem}>
                        <BlueButton text="Dodaj alert" buttonType='button' handleOnClick={handleClickOpen} disabled={addButtonDisabled} />
                        <h1 className={classes.error}>{errorToMuchNotifications}</h1>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Notifications;