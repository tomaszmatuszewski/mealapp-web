import React from 'react';
import Navigation from '../components/menu/Navigation';
import {makeStyles} from '@material-ui/core/styles';
import ViewTitle from '../components/viewTitle/ViewTitle';
import ErrorIcon from '@material-ui/icons/Error';

const useStyles = makeStyles(() => ({
    mainContainer: {
        margin: '15px',
        textAlign: 'center',
    },
    errorIcon: {
        fontSize: '5em',
        color: '#6495ed',
    }
}));

const PageNotFound = () => {
    const classes = useStyles();
    return (
        <div>
            <Navigation menuIcon={true}/>
            <div className={classes.mainContainer}>
            <ErrorIcon className={classes.errorIcon} />
            <ViewTitle title={`Brak strony o podanym adresie`}/>
            </div>
        </div>
    );
}

export default PageNotFound;