import React, { useState, useEffect, useRef, useCallback } from 'react';
import Navigation from '../components/menu/Navigation';
import ViewTitle from '../components/viewTitle/ViewTitle';
import { makeStyles, Theme } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import TextInput from '../components/inputs/TextInput';
import PasswordInput from '../components/inputs/PasswordInput';
import BlueButton from '../components/buttons/BlueButton';
import Validation from '../services/Validation';
import backgroundImg from '../img/loginRegisterForgotPasswordImg.jpg';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: 'rgba(238, 238, 238, 0.81)',
        backgroundImage: `url(${backgroundImg})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundBlendMode: 'color',
        minHeight: '100vh',
        minWidth: '100%',
        paddingBottom: '3%',
    },
    mainContainer: {
        margin: '15px',
        textAlign: 'center',
    },
    flexContainer: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        margin: 'auto',
        alignItems: 'center',
        marginTop: theme.spacing(4),
    },
    flexItem: {
        width: '280px',
        marginBottom: theme.spacing(2),
    },
    lockIcon: {
        color: 'black',
        backgroundColor: 'rgba(243, 150, 154, 0.5)',
        width: '40px',
        height: '40px',
        borderRadius: '30px',
        padding: '5px',
    },
    error: {
        fontSize: '0.8em',
        color: 'red',
    }
}));

const Register = () => {
    const classes = useStyles();
    const firstRender = useRef(true);
    const [disabled, setDisabled] = useState(true);

    const [login, setLogin] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [loginError, setLoginError] = useState('');
    const [emailError, setEmailError] = useState('');
    const [passwordError, setPasswordError] = useState('');

    const [isLoginChanged, setIsLoginChanged] = useState(false);
    const [isEmailChanged, setIsEmailChanged] = useState(false);
    const [isPasswordChanged, setIsPasswordChanged] = useState(false);


    const handleLoginInput = (event: any) => {
        setLogin(event.target.value);
        setIsLoginChanged(true);
    };

    const handleEmailInput = (event: any) => {
        setEmail(event.target.value);
        setIsEmailChanged(true);
    };


    const handlePasswordInput = (event: any) => {
        setPassword(event.target.value);
        setIsPasswordChanged(true);
    };

    const formValidation = useCallback(() => {
        if (isLoginChanged)
            setLoginError(Validation.loginVerification(login));
        if (isEmailChanged)
            setEmailError(Validation.emailVerification(email));
        if (isPasswordChanged)
            setPasswordError(Validation.passwordVerification(password));
        if (loginError.length === 0 && emailError.length === 0 && passwordError.length === 0
            && isLoginChanged && isEmailChanged && isPasswordChanged)
            return false;
        return true;
    }, [isLoginChanged, isEmailChanged, isPasswordChanged, loginError, emailError, passwordError, login, email, password]);

    useEffect(() => {
        if (firstRender.current) {
            firstRender.current = false;
            return
        }
        setDisabled(formValidation())
    }, [login, email, password, formValidation])


    return (
        <div className={classes.root}>
            <Navigation menuIcon={true} />
            <div className={classes.mainContainer} >
                <LockOutlinedIcon className={classes.lockIcon} />
                <ViewTitle title={"Rejestracja"} />
                <form>
                    <div className={classes.flexContainer}>
                        <div className={classes.flexItem}>
                            <TextInput labelText='Login' inputId='loginInput' inputOnChange={handleLoginInput} />
                            <h1 className={classes.error}>{loginError}</h1>
                        </div>
                        <div className={classes.flexItem}>
                            <TextInput labelText='Email' inputId='emailInput' inputOnChange={handleEmailInput} />
                            <h1 className={classes.error}>{emailError}</h1>
                        </div>
                        <div className={classes.flexItem}>
                            <PasswordInput labelText='Hasło' inputId='passwordInput' inputOnChange={handlePasswordInput} />
                            <h1 className={classes.error}>{passwordError}</h1>
                        </div>
                        <div className={classes.flexItem}>
                            <BlueButton text='Zarejestruj się' disabled={disabled} buttonType='submit' />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Register;