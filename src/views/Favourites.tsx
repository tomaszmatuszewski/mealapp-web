import React from 'react';
import Navigation from '../components/menu/Navigation';
import { makeStyles, Theme } from '@material-ui/core/styles';
import ViewTitle from '../components/viewTitle/ViewTitle';
import { useSelector } from 'react-redux';
import {favouritesSelector} from '../store/selectors/favourites.selectors';
import emptyFavourites from '../img/emptyFavourites.png'
import EatingHouse from '../components/eatingHouse/EatingHouse';
import { locationSelector } from '../store/selectors/location.selectors';
import backgroundImg from '../img/favouritesImg.jpg';
import { IMenuProps } from '../services/EatingHousesService';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: 'rgba(238, 238, 238, 0.81)',
        backgroundImage: `url(${backgroundImg})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundBlendMode: 'color',
        minHeight: '100vh',
        minWidth: '100%',
        paddingBottom: '3%',
    },
    mainContainer: {
        margin: '15px',
    },
    dataContainer: {
        margin: '15px auto auto auto',
        maxWidth: '95%',
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
    },
    itemContainer: {
        marginBottom: '-15px',
        margin: '1%',
        width: '600px',
    },
    emptyMainContainer: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        margin: 'auto',
        alignItems: 'center',
    },
    emptyListContainer: {
        width: '280px',
        marginTop: theme.spacing(4),
    },
    icon: {
        width: '150px',
        height: '150px',
    },
    infoContainer: {
        border: '2px solid #f3969a',
        width: '280px',
        borderRadius: '12px',
        backgroundColor: '#ffffff',
    },
}));

const Favourites = () => {
    const classes = useStyles();
    const favourites = useSelector(favouritesSelector);
    const location = useSelector(locationSelector);
    

    const getDailyMenu = (menuList: IMenuProps[]) => {
        let resultArr: IMenuProps[] = []; 
        const date = new Date().toJSON().slice(0, 10);
        menuList.forEach((item) => {
            const splitedDate = item.date.toString().split("T");
            if (splitedDate[0] === date)
                resultArr.push(item);
        })
        return resultArr;
    }

    return (
        <div className={classes.root}>
            <Navigation menuIcon={true} />
            <div className={classes.mainContainer}>
            {(favourites !== null && favourites.favourites.length === 0) ?
                            <div className={classes.emptyMainContainer}>
                                <div className={classes.emptyListContainer}>
                                    <img src={emptyFavourites} className={classes.icon} alt="Brak powiadomień" />
                                </div>
                                <div className={classes.infoContainer}><ViewTitle title={`Coś tutaj pusto...\ndodaj ulubioną jadłodajnie`} /></div>
                            </div>
                            :
                            <div>
                                <ViewTitle title={`Spis twoich ulubionych jadłodajni`} />
                                <div className={classes.dataContainer}>
                                    {!!favourites?.favourites.length && favourites?.favourites.map(favourite => (
                                        <div className={classes.itemContainer} key={favourite.id}>
                                            <EatingHouse
                                                id={favourite.id}
                                                name={favourite.name}
                                                menuList={getDailyMenu(favourite.menuList)}
                                                logoUrl={favourite.logoUrl}
                                                description={favourite.description}
                                                primaryColor={favourite.primaryColor}
                                                slug={favourite.slug}
                                                voivodeship={location.voivodeshipSlug}
                                                city={location.citySlug}
                                                deleteIcon={true}
                                            />
                                        </div>
                                    ))}
                                </div>
                            </div>
                        }
            </div>
        </div>
    );
}

export default Favourites;