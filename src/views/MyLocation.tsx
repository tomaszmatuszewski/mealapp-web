import React, { useState, useCallback } from 'react';
import { useEffect } from 'react';
import Navigation from '../components/menu/Navigation';
import ViewTitle from '../components/viewTitle/ViewTitle';
import locationLogo from '../img/place.png';
import { makeStyles, Theme } from '@material-ui/core/styles';
import BlueButton from '../components/buttons/BlueButton';
import SelectInput from '../components/inputs/SelectInput';
import { NavLink } from 'react-router-dom';
import LocationService, {
    ICitiesProps,
    ICityProps,
    IVoivodeshipProps,
    IVoivodeshipsProps
} from '../services/LocationService';
import CustomLoading from '../components/customLoading/CustomLoading';
import {useSelector} from "react-redux";
import {locationSelector} from "../store/selectors/location.selectors";
import {CurrentLocationService} from "../services/CurrentLocationService";
import {useService} from "../hooks/useService";
import backgroundImg from '../img/myLocationImg.jpg';
import {initialStoreState} from "../store/store";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: 'rgba(238, 238, 238, 0.81)',
        backgroundImage: `url(${backgroundImg})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundBlendMode: 'color',
        minHeight: '100vh',
        minWidth: '100%',
        paddingBottom: '3%',
    },
    mainContainer: {
        margin: '15px',
        textAlign: 'center',
    },
    flexContainer: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        margin: 'auto',
        alignItems: 'center',
    },
    flexItem: {
        width: '280px',
        marginTop: theme.spacing(4),
    },
    logo: {
        width: '200px',
        height: '200px',
        marginTop: '15px',
    },
    navlink: {
        textDecoration: 'none',
    }
}));

const MyLocation = () => {
    const classes = useStyles();
    const location = useSelector(locationSelector);
    const currentLocationService = useService(CurrentLocationService);
    const [voivodeshipChosen, setVoivodeshipChosen] = useState(false);
    const [voivodeships, setVoivodeships] = useState<IVoivodeshipsProps>();
    const [voivodeshipValue, setVoivodeshipValue] = useState<IVoivodeshipProps>({slug: location.voivodeshipSlug, name: location.voivodeshipName });
    const [cityValue, setCityValue] = useState<ICityProps>({
        id: location.cityId,
        slug: location.citySlug,
        name: location.cityName,
        zoomWeb: location.zoom,
        longitude: location.longitude,
        latitude: location.latitude
    });
    const [cities, setCities] = useState<ICitiesProps>();
    const [disabled, setDisabled] = useState(true);
    const [isLoading, setIsLoading] = useState(true);

    const handleSelectVoivodeship = (event: any) => {
        setVoivodeshipValue(event.target.value);
        setCityValue({
            id: -1,
            slug: 'none',
            name: 'none',
            zoomWeb: -1,
            longitude: -1,
            latitude: -1
        });
        currentLocationService.setVoivodeship(event.target.value);
    };

    const handleSelectCity = (event: any) => {
        setCityValue(event.target.value);
        currentLocationService.setCity(event.target.value);
    };

    const formValidation = useCallback(() => {
        if (location.voivodeshipSlug !== 'none')
            setVoivodeshipChosen(true);
        if (location.voivodeshipSlug !== 'none' && location.citySlug !== 'none')
            return false;
        if (location.voivodeshipSlug === 'none')
            setVoivodeshipChosen(false);
        return true;
    }, [location]);

    useEffect(() => {
        LocationService.getVoivodeships().then(resp => {
            if (resp) {
                setVoivodeships(resp);
            }
            setIsLoading(false);
        });
    }, [isLoading]);

    useEffect(() => {
        voivodeshipValue.slug !== 'none' && LocationService.getCities(voivodeshipValue.slug).then(resp => {
            if (resp) {
                setCities(resp);
            }
        });
    }, [voivodeshipValue]);

    useEffect(() => {
        setDisabled(formValidation())
    }, [formValidation]);


    return (
        <div className={classes.root}>
            {location.citySlug === initialStoreState.location.citySlug ? <Navigation menuIcon={false} /> : <Navigation menuIcon={true} />}
            <div className={classes.mainContainer} >
                <ViewTitle title={`Wybierz lokalizację domyślną`} />
                {isLoading ? <CustomLoading /> :
                    <div>
                        <img className={classes.logo} src={locationLogo} alt="location logo" />
                        <form>
                            <div className={classes.flexContainer}>
                                <div className={classes.flexItem}>
                                    <SelectInput
                                        labelId='voivodeshipLabel'
                                        labelText='Województwo'
                                        selectValue={voivodeshipValue.slug}
                                        inputOnChange={handleSelectVoivodeship}
                                        defaultValue={voivodeshipValue.name === location.voivodeshipName && location.voivodeshipName !== 'none' ? location.voivodeshipName : 'Wybierz województwo...'}
                                        items={voivodeships?.voivodeships}
                                    />
                                </div>
                                <div className={classes.flexItem}>
                                    <SelectInput
                                        labelId='cityLabel'
                                        labelText='Miasto'
                                        disabled={!voivodeshipChosen}
                                        selectValue={cityValue.slug}
                                        inputOnChange={handleSelectCity}
                                        defaultValue={cityValue.name === location.cityName && location.cityName !== 'none' ? location.cityName : 'Wybierz miasto...'}
                                        items={cities?.cities}
                                    />
                                </div>
                                <div className={classes.flexItem}>
                                    {disabled ?
                                        <BlueButton disabled={true} text="Przejdź dalej" type='submit'/>
                                        :
                                        <NavLink to={`/eatingHouses/voivodeship/${location.voivodeshipSlug}/place/${location.citySlug}`} className={classes.navlink}>
                                            <BlueButton disabled={false} text="Przejdź dalej" type='submit'/>
                                        </NavLink>
                                    }
                                </div>
                            </div>
                        </form>
                    </div>
                }
            </div>
        </div>
    );
}

export default MyLocation;