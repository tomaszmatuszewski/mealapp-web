import React, { useRef, useState, useEffect, useCallback } from 'react';
import Navigation from '../components/menu/Navigation';
import ViewTitle from '../components/viewTitle/ViewTitle';
import { makeStyles, Theme } from '@material-ui/core/styles';
import AccountIcon from '@material-ui/icons/AccountCircleOutlined';
import TextInput from '../components/inputs/TextInput';
import Validation from '../services/Validation';
import BlueButton from '../components/buttons/BlueButton';
import SelectInput from '../components/inputs/SelectInput';
import backgroundImg from '../img/accountImg.jpg';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: 'rgba(238, 238, 238, 0.81)',
        backgroundImage: `url(${backgroundImg})`,
        backgroundPosition: '25% 65%',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundBlendMode: 'color',
        minHeight: '100vh',
        minWidth: '100%',
        paddingBottom: '3%',
    },
    mainContainer: {
        margin: '15px',
        textAlign: 'center',
    },
    flexContainer: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        margin: 'auto',
        alignItems: 'center',
        marginTop: theme.spacing(4),
    },
    flexItem: {
        width: '280px',
        marginBottom: theme.spacing(2),
    },
    flexItemTopMargin: {
        width: '280px',
        marginTop: theme.spacing(4),
    },
    accountIcon: {
        color: 'black',
        backgroundColor: 'rgba(243, 150, 154, 0.5)',
        width: '40px',
        height: '40px',
        borderRadius: '30px',
        padding: '5px',
    },
    error: {
        fontSize: '0.8em',
        color: 'red',
    }
}));

const Account = () => {
    const classes = useStyles();

    const firstRender = useRef(true);
    const [disabled, setDisabled] = useState(true);

    const [login, setLogin] = useState('');
    const [email, setEmail] = useState('');
    const [loginError, setLoginError] = useState('');
    const [emailError, setEmailError] = useState('');
    const [isLoginChanged, setIsLoginChanged] = useState(false);
    const [isEmailChanged, setIsEmailChanged] = useState(false);
    const [regionValue, setRegionValue] = useState('none');
    const [regionChoosen, setRegionChoosen] = useState(false);
    const [cityValue, setCityValue] = useState('none');

    interface SelectItem {
        label: any,
        value: any
    }

    const regions: SelectItem[] = [
        { label: 'Wybierz województwo...', value: 'none' },
        { label: 'Kujawsko-Pomorskie', value: 'kuj-pom' },
        { label: 'Dolnośląskie', value: 'dol' },
        { label: 'Warmińsko-Mazurskie', value: 'war-maz' },
        { label: 'Wielkopolskie', value: 'wiel' }];
    const cities: SelectItem[] = [
        { label: 'Wybierz miasto...', value: 'none' },
        { label: 'Olsztyn', value: 'olsztyn' },
        { label: 'Ełk', value: 'ełk' },
        { label: 'Braniewo', value: 'braniewo' },
        { label: 'Szczytno', value: 'szczytno' },
        { label: 'Barczewo', value: 'barczewo' }];

    const handleLoginInput = (event: any) => {
        setLogin(event.target.value);
        setIsLoginChanged(true);
    };

    const handleEmailInput = (event: any) => {
        setEmail(event.target.value);
        setIsEmailChanged(true);
    };

    const handleSelectRegion = (event: any) => {
        setRegionValue(event.target.value);
        setCityValue('none');
    }

    const handleSelectCity = (event: any) => {
        setCityValue(event.target.value);
    }

    const formValidation = useCallback(() => {
        if (isLoginChanged)
            setLoginError(Validation.loginVerification(login));
        if (isEmailChanged)
            setEmailError(Validation.emailVerification(email));
        if (regionValue !== 'none')
            setRegionChoosen(true);
        if (regionValue === 'none')
            setRegionChoosen(false);
        if (loginError.length === 0 && emailError.length === 0 && isLoginChanged && isEmailChanged && regionValue !== 'none' && cityValue !== 'none')
            return false;
        return true;
    }, [isLoginChanged, isEmailChanged, loginError, emailError, login, email, regionValue, cityValue]);

    useEffect(() => {
        if (firstRender.current) {
            firstRender.current = false;
            return
        }
        setDisabled(formValidation())
    }, [formValidation])

    return (
        <div className={classes.root}>
            <Navigation menuIcon={true} />
            <div className={classes.mainContainer}>
                <AccountIcon className={classes.accountIcon} />
                <ViewTitle title='Edytuj profil' />
                <form>
                    <div className={classes.flexContainer}>
                        <div className={classes.flexItem}>
                            <TextInput labelText='Login' inputId='loginInput' inputOnChange={handleLoginInput} />
                            <h1 className={classes.error}>{loginError}</h1>
                        </div>
                        <div className={classes.flexItem}>
                            <TextInput labelText='Email' inputId='emailInput' inputOnChange={handleEmailInput} />
                            <h1 className={classes.error}>{emailError}</h1>
                        </div>
                        <div className={classes.flexItemTopMargin}>
                            <SelectInput 
                            labelId='regionLabel' labelText='Województwo' selectValue={regionValue} inputOnChange={handleSelectRegion} items={regions} />
                        </div>
                        <div className={classes.flexItemTopMargin}>
                            <SelectInput labelId='cityLabel' labelText='Miasto' disabled={!regionChoosen} selectValue={cityValue} inputOnChange={handleSelectCity} items={cities} />
                        </div>
                        <div className={classes.flexItemTopMargin}>
                            <BlueButton disabled={disabled} text='Zapisz zmiany' buttonType='submit' />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Account;