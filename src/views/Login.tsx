import React, { useState, useEffect } from 'react';
import Navigation from '../components/menu/Navigation';
import ViewTitle from '../components/viewTitle/ViewTitle';
import { IconButton, Tooltip } from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import BlueButton from '../components/buttons/BlueButton';
import { NavLink } from 'react-router-dom';
import TextInput from '../components/inputs/TextInput';
import PasswordInput from '../components/inputs/PasswordInput';
import backgroundImg from '../img/loginRegisterForgotPasswordImg.jpg';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: 'rgba(238, 238, 238, 0.81)',
        backgroundImage: `url(${backgroundImg})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundBlendMode: 'color',
        minHeight: '100vh',
        minWidth: '100%',
        paddingBottom: '3%',
    },
    mainContainer: {
        margin: '15px',
    },
    dataContainer: {
        margin: 'auto',
        textAlign: 'center',
    },
    lockIcon: {
        color: 'black',
        backgroundColor: 'rgba(243, 150, 154, 0.5)',
        width: '40px',
        height: '40px',
        borderRadius: '30px',
        padding: '5px',
    },
    form: {
        marginTop: theme.spacing(4),
    },
    flexContainer: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        width: '280px',
        margin: 'auto',
    },
    bottomMargin: {
        marginBottom: theme.spacing(2),
    },
    smallFont: {
        fontSize: '1em',
        fontWeight: 'unset',
    },
    loginsContainer: {
        margin: 'auto',
        marginBottom: theme.spacing(1),
    },
    iconGoogle: {
        marginLeft: theme.spacing(2),
        width: '2.5em',
        height: '2.5em',
        fontSize: '2.5em',
        color: 'red',
    },
    iconFacebook: {
        marginRight: theme.spacing(2),
        width: '2.5em',
        height: '2.5em',
        fontSize: '2.5em',
        color: 'blue',
    },
    navlink: {
        textDecoration: 'none',
    }
}));

const Login = () => {
    const classes = useStyles();
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [buttonEnabled, setButtonEnabled] = useState(false);

    const handleLoginInput = (event: any) => {
        setLogin(event.target.value);
    };

    const handlePasswordInput = (event: any) => {
        setPassword(event.target.value);
    };

    useEffect(() => {
        if (login !== '' && password !== '')
            setButtonEnabled(true);
        else
            setButtonEnabled(false);
    }, [password, login, setButtonEnabled])

    return (
        <div className={classes.root}>
            <Navigation menuIcon={true} />
            <div className={classes.mainContainer}>
                <div className={classes.dataContainer}>
                    <LockOutlinedIcon className={classes.lockIcon} />
                    <ViewTitle title={"Logowanie"} />
                    <form>
                        <div className={classes.form}>
                            <div className={classes.flexContainer}>
                                <div className={classes.bottomMargin}>
                                    <TextInput labelText='Login' inputId='loginInput' inputOnChange={handleLoginInput} />
                                </div>
                                <div className={classes.bottomMargin}>
                                    <PasswordInput labelText='Hasło' inputId='passwordInput' inputOnChange={handlePasswordInput} />
                                </div>
                                <h1 className={classes.smallFont}>Inne opcje logowania</h1>
                            </div>
                            <div className={classes.loginsContainer}>
                                <Tooltip title="Logowanie przez Facebook">
                                    <IconButton className={classes.iconFacebook}>
                                        <i className="fa fa-facebook" aria-hidden="true"></i>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title="Logowanie przez Google">
                                    <IconButton className={classes.iconGoogle}>
                                        <i className="fa fa-google-plus" aria-hidden="true"></i>
                                    </IconButton>
                                </Tooltip>
                            </div>
                            <BlueButton text='Zaloguj się' disabled={!buttonEnabled} buttonType='submit'/>
                        </div>
                    </form>
                    <div className={classes.flexContainer}>
                        <h1 className={classes.smallFont}>Nie masz konta?<br />Zarejestruj się</h1>
                    </div>
                    <div className={classes.flexContainer}>
                        <NavLink to='/account/register' className={classes.navlink}>
                            <BlueButton text='Zarejestruj się' type='button' />
                        </NavLink>
                    </div>
                    <div className={classes.flexContainer}>
                        <h1 className={classes.smallFont}>Zapomniałeś hasła? <NavLink to="/account/forgotPassword">Kliknij tutaj</NavLink></h1>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;