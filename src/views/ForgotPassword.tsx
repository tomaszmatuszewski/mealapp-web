import React, { useState, useEffect, useCallback, useRef } from 'react';
import Navigation from '../components/menu/Navigation';
import ViewTitle from '../components/viewTitle/ViewTitle';
import { makeStyles, Theme } from '@material-ui/core/styles';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import TextInput from '../components/inputs/TextInput';
import Validation from '../services/Validation';
import BlueButton from '../components/buttons/BlueButton';
import backgroundImg from '../img/loginRegisterForgotPasswordImg.jpg';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: 'rgba(238, 238, 238, 0.81)',
        backgroundImage: `url(${backgroundImg})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundBlendMode: 'color',
        minHeight: '100vh',
        minWidth: '100%',
        paddingBottom: '3%',
    },
    mainContainer: {
        margin: '15px',
        textAlign: 'center',
    },
    flexContainer: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        margin: 'auto',
        alignItems: 'center',
        width: '280px',
    },
    mailIcon: {
        color: 'black',
        backgroundColor: 'rgba(243, 150, 154, 0.5)',
        width: '40px',
        height: '40px',
        borderRadius: '30px',
        padding: '5px',
    },
    info: {
        fontSize: '1em',
        fontWeight: 'unset',
    },
    error: {
        fontSize: '0.8em',
        color: 'red',
    }
}));

const ForgotPassword = () => {
    const classes = useStyles();

    const firstRender = useRef(true);
    const [disabled, setDisabled] = useState(true);
    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState('');

    const handleEmailInput = (event: any) => {
        setEmail(event.target.value);
    };

    const formValidation = useCallback(() => {
        setEmailError(Validation.emailVerification(email));
        if (emailError.length === 0)
            return false;
        else
            return true;
    }, [emailError, email]);

    useEffect(() => {
        if (firstRender.current) {
            firstRender.current = false;
            return
        }
        setDisabled(formValidation())
    }, [email, formValidation])

    return (
        <div className={classes.root}>
            <Navigation menuIcon={true} />
            <div className={classes.mainContainer}>
                <MailOutlineIcon className={classes.mailIcon} />
                <ViewTitle title={"Zapomniałem hasła"} />
                <form>
                    <div className={classes.flexContainer}>
                        <h1 className={classes.info}>Podaj email na który wyślemy wygenerowane hasło</h1>
                        <TextInput labelText='Email' inputId='emailInput' inputOnChange={handleEmailInput} />
                        <h1 className={classes.error}>{emailError}</h1>
                        <BlueButton text='Wyślij' buttonType='submit' disabled={disabled} />
                    </div>
                </form>
            </div>
        </div>
    );
}

export default ForgotPassword;