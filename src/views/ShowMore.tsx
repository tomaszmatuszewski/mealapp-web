import React, { useState, useEffect } from 'react';
import Navigation from '../components/menu/Navigation';
import { makeStyles, Theme } from '@material-ui/core/styles';
import ShowMoreService, { IShowMoreProps } from '../services/ShowMoreService';
import CustomLoading from '../components/customLoading/CustomLoading';
import ViewTitle from '../components/viewTitle/ViewTitle';
import { Typography, Tooltip, IconButton } from '@material-ui/core';
import ArrowLeftIcon from '@material-ui/icons/ArrowBackSharp';
import ArrowRightIcon from '@material-ui/icons/ArrowForwardSharp';
import Dinner from '../components/dinner/Dinner';
import LocationIcon from '@material-ui/icons/LocationOnSharp';
import PhoneIcon from '@material-ui/icons/PhoneSharp';
import ClockIcon from '@material-ui/icons/WatchLaterSharp';
import { useParams } from 'react-router';
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";
import Connection from '../services/Connection'
import StarIcon from '@material-ui/icons/StarsSharp';
import backgroundImg from '../img/showMoreImg.jpg';
import { useService } from '../hooks/useService';
import FavouritesService from '../services/FavouritesService';
import { useSelector } from 'react-redux';
import { favouritesSelector } from '../store/selectors/favourites.selectors';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: 'rgba(238, 238, 238, 0.81)',
        backgroundImage: `url(${backgroundImg})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundBlendMode: 'color',
        minHeight: '100vh',
        minWidth: '100%',
        paddingBottom: '3%',
    },
    mainContainer: {
        display: 'flex',
        width: '100%',
        flexDirection: 'column',
        textAlign: 'center',
        alignItems: 'center',

    },
    topContainer: {
        height: '200px',
        width: '100%',
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        margin: 'auto',
        display: 'flex',
        justifyContent: 'center',
        borderTop: 'solid 5px #6495ed',
        borderBottom: 'solid 5px #6495ed',
    },
    titleContainer: {
        backgroundColor: 'rgba(100, 149, 237, 0.8)',
        margin: 'auto',
        padding: '0',
        paddingTop: '0.5%',
        paddingBottom: '0.5%',
        paddingLeft: '1%',
        borderRadius: '15px',
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
        alignItems: 'center',
        maxWidth: '99%',

    },
    logoContainer: {
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        marginRight: '0.5%',
    },
    logo: {
        width: '100px',
        height: '100px',
        borderRadius: '50px',
    },
    title: {
        color: '#ffffff',
        fontSize: '2em',
    },
    starIconFavourite: {
        color: 'gold',
        fontSize: '1.5em',
    },
    starIconNotFavourite: {
        color: '#f3969a',
        fontSize: '1.5em',
    },
    topMargin: {
        marginTop: theme.spacing(2),
    },
    bottomBorder: {
        borderBottom: '3px solid #f3969a',
    },
    emptyMenuListContainer: {
        borderBottom: '3px solid #f3969a',
        width: '300px',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    menuListContainer: {
        marginTop: '1%',
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        display: 'flex',
        width: '100%',
        justifyContent: 'center',
    },
    infoContainer: {
        minWidth: '300px',
        maxWidth: '600px',
        margin: 'auto',
        backgroundColor: '#ffffff',
        border: '3px solid #f3969a',
        borderRadius: '12px',
        marginTop: '1%',
        padding: '1%',
        marginLeft: '3%',
        marginRight: '3%',
    },
    tagContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        minWidth: '300px',
        maxWidth: '600px',
        margin: 'auto',
        backgroundColor: '#ffffff',
        border: '3px solid #f3969a',
        borderRadius: '12px',
        marginTop: '1%',
        padding: '1%',
        marginLeft: '3%',
        marginRight: '3%',
    },
    tagItem: {
        margin: '2%',
        display: 'flex',
        padding: theme.spacing(1),
        minHeight: '40px',
        borderRadius: '20px',
        borderWidth: '2px',
        borderStyle: 'solid',
        alignItems: 'center',
    },
    icon: {
        color: '#6495ed',
        fontSize: '3em',
    },
    leftArrow: {
        color: '#6495ed',
        fontSize: '4em',
        marginTop: 'auto',
        marginBottom: 'auto',
        marginRight: theme.spacing(2),
        "&:hover": {
            color: '#f3969a',
            cursor: 'pointer',
        }
    },
    rightArrow: {
        color: '#6495ed',
        fontSize: '4em',
        marginTop: 'auto',
        marginBottom: 'auto',
        marginLeft: theme.spacing(2),
        "&:hover": {
            color: '#f3969a',
            cursor: 'pointer',
        }
    },
    dateAndMealsContainer: {
        width: '300px',
    },
    pointsListContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: 'auto',
        justifyContent: 'center',
        width: '95%',
    },
    pointContainer: {
        padding: '1%',
        margin: '1%',
        backgroundColor: '#ffffff',
        maxWidth: '600px',
        border: '3px solid #f3969a',
        borderRadius: '12px',
    },
    addressContainer: {
        display: 'flex',
        maxWidth: '600px',
        flexWrap: 'wrap',
        justifyContent: 'center',

    },
    rowContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        margin: 'auto',
        width: '300px',
    },
    columnContainer: {
        marginTop: theme.spacing(2),
        alignItems: 'center',
        width: '300px',
        display: 'flex',
        flexDirection: 'column',
    },
    textNearIcon: {
        marginLeft: theme.spacing(4),
    },
    map: {
        marginTop: theme.spacing(2),
        width: '250px',
        height: '200px',
        border: '2px solid #f3969a',
    },
}));

const mapControls = {
    mapTypeControl: false,
    streetViewControl: false,
    fullscreenControl: false,
    clickableIcons: false,
};

const ShowMore = () => {
    const classes = useStyles();
    const [eatingHouseMore, setEatingHouseMore] = useState<IShowMoreProps | null>(null);
    const [staticMenu, setStaticMenu] = useState<string>('');
    const [isLoading, setIsLoading] = useState(true);
    const { slug, voivodeshipName, cityName } = useParams();
    const [dishCounter, setDishCounter] = useState(0);
    const favouritesService = useService(FavouritesService);
    const favourites = useSelector(favouritesSelector);
    const [isInFavourites, setIsInFavourites] = useState<boolean>(false);
    const colors = ['crimson', 'darkgreen', 'darkmagenta', 'darkorange', 'darkturquoise', 'hotpink'];

    function convertDateToString(date: Date) {
        let result = date.toString();
        result = result.substring(0, 10);
        return result;
    }

    const handleLeftArrowClick = () => {
        if (eatingHouseMore && dishCounter > 0)
            setDishCounter(dishCounter - 1);
    }

    const handleRightArrowClick = () => {
        if (eatingHouseMore && dishCounter < eatingHouseMore?.menuList.length - 1)
            setDishCounter(dishCounter + 1);
    }

    const handleAddToFavourites = () => {
        if (!isInFavourites && eatingHouseMore !== null) {
            setIsInFavourites(true);
            favouritesService.addToFavourites(eatingHouseMore);
        }
    }

    useEffect(() => {
        ShowMoreService.getShowMore(slug, voivodeshipName, cityName).then(resp => {
            if (resp) {
                setEatingHouseMore(resp);
                console.log(resp);
                const date = new Date().toJSON().slice(0, 10);
                resp.menuList.forEach((item, dataIndex) => {
                    const splitedDate = item.date.toString().split("T");
                    if (splitedDate[0] === date) {
                        setDishCounter(dataIndex);
                        item.contentList.forEach(contentItem => {
                            if (contentItem.type === "STATIC")
                                setStaticMenu(contentItem.content);
                        })
                    }
                })
                favourites.favourites.forEach(favourite => {
                    if (favourite.id === resp.id)
                        setIsInFavourites(true);
                })
            }
            setIsLoading(false);
        });
    }, [slug, voivodeshipName, cityName, favourites.favourites, isLoading])

    return (
        <div className={classes.root}>
            <Navigation menuIcon={true} />
            <div className={classes.mainContainer}>
                {isLoading ? <CustomLoading />
                    : (eatingHouseMore === null) ?
                        <ViewTitle title={`Brak danych o jadłodajni`} />
                        :
                        <div className={classes.mainContainer}>
                            <div className={classes.topContainer} style={{ backgroundImage: `url(${eatingHouseMore.photoUrl})` }}>
                                <div className={classes.titleContainer}>
                                    <div className={classes.logoContainer}>
                                        <img src={eatingHouseMore.logoUrl} alt="Logo restauracji" className={classes.logo} />
                                    </div>
                                    <Typography variant="h5" className={classes.title}>{eatingHouseMore.name}</Typography>
                                    <Tooltip title={isInFavourites ?
                                        "Jadłodajnia znajduje się już w ulubionych" : "Dodaj do ulubionych"}>
                                        <div>
                                            <IconButton disabled={isInFavourites} onClick={handleAddToFavourites}>
                                                <StarIcon className={isInFavourites ? classes.starIconFavourite : classes.starIconNotFavourite} />
                                            </IconButton>
                                        </div>
                                    </Tooltip>
                                </div>
                            </div>
                            <Typography variant="h5" className={classes.topMargin}>Aktualności</Typography>
                            {(eatingHouseMore.menuList[dishCounter] !== undefined) ?
                                <div className={classes.menuListContainer}>
                                    <ArrowLeftIcon className={classes.leftArrow} onClick={handleLeftArrowClick} />
                                    <div className={classes.dateAndMealsContainer}>
                                        <div className={classes.bottomBorder}>
                                            <Typography variant="h6">{convertDateToString(eatingHouseMore.menuList[dishCounter].date)}</Typography>
                                        </div>
                                        <div>
                                            {eatingHouseMore.menuList[dishCounter].contentList.map((contentItem) => (
                                                (contentItem.type === 'DAILY') ?
                                                    <Dinner key={contentItem.id} content={contentItem.content} /> :
                                                    null
                                            ))}
                                        </div>
                                    </div>
                                    <ArrowRightIcon className={classes.rightArrow} onClick={handleRightArrowClick} />
                                </div>
                                :
                                <div className={classes.infoContainer}>
                                    <Typography variant="body1">Ta jadłodajnia nie udostępnia aktualności</Typography>
                                </div>
                            }
                            <Typography variant="h5" className={classes.topMargin}>Menu główne</Typography>
                            <div className={classes.infoContainer}>
                                {staticMenu !== '' ?
                                    <Typography variant="body1">{staticMenu}</Typography>
                                    :
                                    <Typography variant="body1">Ta jadłodajnia nie udostępnia menu głównego</Typography>
                                }
                            </div>
                            <Typography variant="h5" className={classes.topMargin}>Tagi</Typography>
                            {eatingHouseMore.eatingHouseTagList.length !== 0 ?
                                <div className={classes.tagContainer}>
                                    {eatingHouseMore.eatingHouseTagList.map((tag, index) => {
                                        let colorIndex = Math.floor(Math.random() * (Math.floor(6) - Math.ceil(0))) + Math.ceil(0);
                                        return (
                                            <div key={index} className={classes.tagItem} style={{ borderColor: colors[colorIndex] }}>
                                                <Typography variant="body1">{tag.name}</Typography>
                                            </div>)
                                    })}
                                </div>
                                :
                                <div className={classes.infoContainer}>
                                    <Typography variant="body1">Brak informacji o tagach</Typography>
                                </div>
                            }
                            <Typography variant="h5" className={classes.topMargin}>Dostępne punkty</Typography>
                            <div className={classes.pointsListContainer}>
                                {eatingHouseMore.addressList.map((address, index) => (
                                    <div key={address.id} className={classes.pointContainer}>
                                        <Typography variant="h5">Punkt {index + 1}</Typography>
                                        <div className={classes.addressContainer}>
                                            <div className={classes.columnContainer}>
                                                <Typography variant="h6">Godziny otwarcia</Typography>
                                                {(address.openHoursList.length !== 0) ?
                                                    <div className={classes.rowContainer}>
                                                        <ClockIcon className={classes.icon} />
                                                        <div>
                                                            {address.openHoursList.map((openHoursItem, index) => (
                                                                (openHoursItem === null) ? <Typography variant="body1">Brak danych</Typography>
                                                                    :
                                                                    <div key={index}>
                                                                        <Typography variant="body1" className={classes.textNearIcon}>{openHoursItem.day}: {openHoursItem.fromHour}-{openHoursItem.toHour}</Typography>
                                                                    </div>
                                                            ))}
                                                        </div>
                                                    </div>
                                                    :
                                                    <div className={classes.rowContainer}>
                                                        <Typography variant="body1">Brak informacji o godzinach otwarcia</Typography>
                                                    </div>
                                                }
                                            </div>
                                            <div className={classes.columnContainer}>
                                                <Typography variant="h6">Mapa</Typography>
                                                {address.latitude !== null && address.longitude !== null ?
                                                    <div className={classes.rowContainer}>
                                                        <LoadScript
                                                            googleMapsApiKey={Connection.getMap()}
                                                            loadingElement={<CustomLoading />}
                                                        >
                                                            <GoogleMap
                                                                zoom={16}
                                                                center={{
                                                                    lat: address.latitude,
                                                                    lng: address.longitude
                                                                }}
                                                                mapContainerClassName={classes.map}
                                                                options={mapControls}
                                                            >
                                                                <Marker
                                                                    key={address.id}
                                                                    position={{
                                                                        lat: address.latitude,
                                                                        lng: address.longitude
                                                                    }}
                                                                    title={`${eatingHouseMore.name}`}
                                                                    // icon={<LocationIcon className={classes.icon}/>}
                                                                />
                                                            </GoogleMap>
                                                        </LoadScript>
                                                    </div>
                                                    :
                                                    <div className={classes.rowContainer}>
                                                        <Typography variant="body1">Brak informacji o lokalizacji</Typography>
                                                    </div>
                                                }
                                            </div>
                                            <div className={classes.columnContainer}>
                                                <Typography variant="h6">Kontakt</Typography>
                                                {(address.phoneNumber !== '') ?
                                                    <div className={classes.rowContainer}>
                                                        <PhoneIcon className={classes.icon} />
                                                        <Typography variant="body1" className={classes.textNearIcon}>{address.phoneNumber}</Typography>
                                                    </div>
                                                    :
                                                    <div className={classes.rowContainer}>
                                                        <Typography variant="body1">Brak informacji o numerze telefonu</Typography>
                                                    </div>
                                                }
                                            </div>
                                            <div className={classes.columnContainer}>
                                                <Typography variant="h6">Adres</Typography>
                                                {(address.address !== '') ?
                                                    <div className={classes.rowContainer}>
                                                        <LocationIcon className={classes.icon} />
                                                        <Typography variant="body1" className={classes.textNearIcon}>{address.address}, {address.city}</Typography>
                                                    </div>
                                                    :
                                                    <div className={classes.rowContainer}>
                                                        <Typography variant="body1">Brak informacji o adresie</Typography>
                                                    </div>
                                                }
                                            </div>

                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                }


            </div>
        </div>
    );
}

export default ShowMore;