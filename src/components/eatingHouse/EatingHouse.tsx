import React from 'react';
import { Card, Typography, Tooltip } from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { IMenuProps } from '../../services/EatingHousesService'
import Dinner from '../dinner/Dinner';
import BlueButton from '../buttons/BlueButton';
import { NavLink } from 'react-router-dom';
import DeleteIcon from '@material-ui/icons/HighlightOffSharp';
import { useService } from '../../hooks/useService';
import FavouritesService from '../../services/FavouritesService';

const useStyles = makeStyles((theme: Theme) => ({
    card: {
        border: '2px solid #f3969a',
        marginBottom: theme.spacing(5),
        minHeight: '200px',
    },
    content: {
        padding: '2%',
        display: 'flex',
        flexDirection: 'column',
    },
    topPanel: {
        display: 'flex',
    },
    logoContainer: {
        border: '2px solid #f3969a',
        borderRadius: '12px',
        width: '100px',
        height: '100px',
    },
    logo: {
        width: '100px',
        height: '100px',
        borderRadius: '12px',
    },
    titleContainer: {
        display: 'flex',
        width: '100%',
        marginTop: 'auto',
        marginBottom: 'auto',
        borderBottom: '2px solid transparent',
        borderImage: 'linear-gradient(to left, #6495ed, #f3969a)',
        borderImageSlice: 1,
        justifyContent: 'space-between',
    },
    deleteIcon: {
        color: '#f3969a',
        fontSize: '2.5em',
        cursor: 'pointer',

        '&:hover': {
            color: 'red',
        }
    },
    titleRow: {
        marginTop: 'auto',
        marginBottom: 'auto',
        marginLeft: '3%',
    },
    subtitle: {
        fontSize: '1.2em',
    },
    dividerPink: {
        textAlign: 'center',
        borderBottom: '2px solid #f3969a'
    },
    dinnersContainer: {
        marginBottom: theme.spacing(2),
    },
    noDailyContainer: {
        textAlign: 'center',
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    buttonContainer: {
        textAlign: 'center',
    },
    navlink: {
        textDecoration: 'none',
    }
}));


const EatingHouse = (props: {
    id: number;
    name: string;
    menuList: IMenuProps[];
    logoUrl: string;
    description: string;
    primaryColor: string;
    slug: string;
    voivodeship: string;
    city: string;
    deleteIcon: boolean
}) => {

    const classes = useStyles();
    const favouritesService = useService(FavouritesService);

    const handleDeleteFromFavourites = (id: number) => {
        favouritesService.deleteFromFavourites(id);
    }

    return (
        <Card className={classes.card}>
            <div className={classes.content}>
                <div className={classes.topPanel}>
                    <div className={classes.logoContainer}>
                        <img src={props.logoUrl} className={classes.logo} alt="Logo restauracji" />
                    </div>

                    <div className={classes.titleContainer}>
                        <div className={classes.titleRow}>
                            <Typography variant="h6">{props.name}</Typography>
                        </div>
                        {props.deleteIcon ?
                            <Tooltip title={"Usuń z ulubionych"} onClick={() => handleDeleteFromFavourites(props.id)}>
                                <DeleteIcon className={classes.deleteIcon} />
                            </Tooltip>
                            : null}
                    </div>
                </div>

                <div className={classes.dividerPink}>
                    <Typography variant="subtitle1" className={classes.subtitle}>
                        Dzisiejsze zestawy
                        </Typography>
                </div>

                <div className={classes.dinnersContainer}>

                    {


                        props.menuList.map(menuListItem =>
                            menuListItem.contentList.map(contentListItem =>
                                (contentListItem.type === "DAILY") ?
                                    <Dinner key={contentListItem.id} content={contentListItem.content} />
                                    : null)
                        )
                    }

                </div>

                <div className={classes.buttonContainer}>
                    <NavLink to={`/eatingHouses/${props.slug}/voivodeship/${props.voivodeship}/place/${props.city}`} className={classes.navlink}>
                        <BlueButton buttonType='button' text='Pokaż więcej' />
                    </NavLink>
                </div>
            </div>
        </Card>
    );
}

export default EatingHouse;