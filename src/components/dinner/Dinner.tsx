import React from 'react';
import { Typography } from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        borderBottom: '2px solid #f3969a',
        textAlign: 'center',
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    breakLine: {
        whiteSpace: 'pre-line'
    }
}));

const Dinner = (props: { content: string; }) => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <Typography variant="body1" className={classes.breakLine}>
                {props.content}
            </Typography>
        </div>
    );
}

export default Dinner;