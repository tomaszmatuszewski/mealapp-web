import React, {useState} from 'react';
import MenuBar from './MenuBar';
import DrawerComponent from './DrawerComponent';

const Navigation = ({menuIcon}: any) => {
    const [drawerOpen, setDrawerOpen] = useState(false);

    return (
        <div>
            <MenuBar hasMenuIcon={menuIcon} onIconClick={() => setDrawerOpen(true)}/>
            <DrawerComponent shouldBeOpen={drawerOpen} onBackgroundClick={() => setDrawerOpen(false)} />
        </div>
    );
}

export default Navigation;