import React from 'react';
import { AppBar, IconButton, Toolbar, Typography, Container, Tooltip } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import AccountIcon from '@material-ui/icons/AccountCircle';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom';
import logo from '../../img/logo.png';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
        display: 'flex',
    },
    appBar: {
        background: '#6495ed',
    },
    container: {
        maxWidth: '1460px',
    },
    title: {
        flexGrow: 1,
    },
    logo: {
        marginLeft: '3%',
        marginRight: '1%',
        height: '30px',
        width: '30px',
    },
    menuButtonVisible: {
        opacity: 1,
    },
    menuButtonHide: {
        opacity: 0,
    }
}));

interface IMenuBarProps {
    onIconClick: () => void;
    hasMenuIcon: boolean;
}

const MenuBar: React.FC<IMenuBarProps> = ({ onIconClick, hasMenuIcon }) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position='static' className={classes.appBar}>
                <Container className={classes.container}>
                    <Toolbar>
                            <Tooltip title="Menu">
                                <IconButton
                                    edge="start"
                                    color="inherit"
                                    aria-label="menu"
                                    onClick={() => onIconClick()}
                                    className={hasMenuIcon ? classes.menuButtonVisible : classes.menuButtonHide}
                                >
                                    <MenuIcon />
                                </IconButton>
                            </Tooltip>
                        <img src={logo} alt="Logo MealApp" className={classes.logo}/>
                        <Typography
                            variant="h6"
                            className={classes.title}
                        >
                            MealApp
                    </Typography>
                        <Tooltip title="Konto">
                            <IconButton
                                color='inherit'
                                component={NavLink}
                                to='/account/login'
                            >
                                <AccountIcon />
                            </IconButton>
                        </Tooltip>
                    </Toolbar>
                </Container>
            </AppBar>
        </div>
    );
}

export default MenuBar;