import React from 'react';
import { Drawer, Typography } from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import RestaurantIcon from '@material-ui/icons/Restaurant';
import StarIcon from '@material-ui/icons/Star';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MapIcon from '@material-ui/icons/Map';
import HomeIcon from '@material-ui/icons/Home';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { locationSelector } from '../../store/selectors/location.selectors';

const useStyles = makeStyles((theme: Theme) => ({
    itemTextAndIcon: {
        color: '#f7f7f7',
    },
    drawer: {
        backgroundImage: 'linear-gradient(to bottom, #6495ed, #f3969a)',
        width: '250px',
        alignContent: 'center',
    },
    listItem: {
        transition: 'all 0.3s ease-in-out',

        "&:hover": {
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
        }
    },
    listItemActive: {
        transition: 'all 0.3s ease-in-out',
        backgroundColor: 'rgba(243, 150, 154, 0.8)',

        "&:hover": {
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
        }
    },
    divider: {
        height: '2px',
        backgroundColor: '#f7f7f7',
    },
    menuTitle: {
        marginTop: theme.spacing(3),
        color: '#f7f7f7',
    }
}));

interface IDrawerComponentProps {
    shouldBeOpen: boolean;
    onBackgroundClick: () => void;
}

const DrawerComponent: React.FC<IDrawerComponentProps> = ({ shouldBeOpen, onBackgroundClick }) => {
    const classes = useStyles();
    const location = useSelector(locationSelector);

    return (
        <div>
            <Drawer
                open={shouldBeOpen}
                classes={{ paper: classes.drawer }}
                ModalProps={{ onBackdropClick: () => onBackgroundClick() }}
            >
                <Typography
                    variant="h6"
                    align='center'
                    className={classes.menuTitle}
                >
                    Menu
                </Typography>
                <List>
                    <Divider className={classes.divider} />
                    <ListItem className={classes.listItem} component={NavLink} exact to={``} activeClassName={classes.listItemActive} button onClick={() => onBackgroundClick()}>
                        <ListItemIcon className={classes.itemTextAndIcon}><HomeIcon /></ListItemIcon>
                        <ListItemText className={classes.itemTextAndIcon} primary="Lokalizacja" />
                    </ListItem>
                    <ListItem className={classes.listItem} component={NavLink} exact to={`/eatingHouses/voivodeship/${location.voivodeshipSlug}/place/${location.citySlug}`} activeClassName={classes.listItemActive} button onClick={() => onBackgroundClick()}>
                        <ListItemIcon className={classes.itemTextAndIcon}><RestaurantIcon /></ListItemIcon>
                        <ListItemText className={classes.itemTextAndIcon} primary="Jadłodajnie" />
                    </ListItem>
                    <ListItem className={classes.listItem} component={NavLink} to='/favourites' activeClassName={classes.listItemActive} button onClick={() => onBackgroundClick()}>
                        <ListItemIcon className={classes.itemTextAndIcon}><StarIcon /></ListItemIcon>
                        <ListItemText className={classes.itemTextAndIcon} primary="Ulubione" />
                    </ListItem>
                    <ListItem className={classes.listItem} component={NavLink} to='/notifications' activeClassName={classes.listItemActive} button onClick={() => onBackgroundClick()}>
                        <ListItemIcon className={classes.itemTextAndIcon}><NotificationsIcon /></ListItemIcon>
                        <ListItemText className={classes.itemTextAndIcon} primary="Powiadomienia" />
                    </ListItem>
                    <ListItem className={classes.listItem} component={NavLink} to='/map' activeClassName={classes.listItemActive} button onClick={() => onBackgroundClick()}>
                        <ListItemIcon className={classes.itemTextAndIcon}><MapIcon /></ListItemIcon>
                        <ListItemText className={classes.itemTextAndIcon} primary="Mapa" />
                    </ListItem>
                    <Divider light={true} className={classes.divider} />
                </List>
            </Drawer>
        </div>
    );
}

export default DrawerComponent;