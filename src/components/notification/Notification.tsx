import React from 'react';
import DeleteIcon from '@material-ui/icons/HighlightOff';
import { Switch, Typography, makeStyles, IconButton, Divider, Tooltip } from '@material-ui/core';

const useStyles = makeStyles(() => ({
    notificationContainer: {
        width: '100%',
        border: '2px solid #f3969a',
        wordWrap: 'break-word',
        height: '90px',
    },
    labelContainer: {
        margin: 'auto',
        textAlign: 'center',
    },
    iconButton: {
        padding: '0',
    },
    deleteIcon: {
        color: '#f3969a',
        fontSize: '1.5em',
    },
    switchBase: {
        color: '#f3969a',
        '&$checked': {
            color: '#6495ed',
        },
        '&$checked + $track': {
            backgroundColor: '#6495ed',
        },
    },
    checked: {},
    track: {},
    dividerGradient: {
        height: '2px',
        backgroundImage: 'linear-gradient(to left, #6495ed, #f3969a)',
    },
    row: {
        display: 'flex',
        justifyContent: 'space-between',
    }
}));

const Notification = (props: { id: number, label: string, isOn: boolean, checkHandler: (id: number) => void, deleteHandler: (id: number) => void}) => {
    const classes = useStyles();


    return (
        <div className={classes.notificationContainer}>
            <div className={classes.row}>
                <div>
                    <Tooltip title="Usuń alert" onClick={() => { }}>
                        <IconButton className={classes.iconButton}>
                            <DeleteIcon className={classes.deleteIcon} onClick={() => props.deleteHandler(props.id)}/>
                        </IconButton>
                    </Tooltip>
                </div>

                <div>
                    <Tooltip title={props.isOn ? "Wyłącz alert" : "Włącz alert"}>
                        <Switch checked={props.isOn} classes={{ switchBase: classes.switchBase, checked: classes.checked, track: classes.track, }} onChange={() => props.checkHandler(props.id)} />
                    </Tooltip>
                </div>
            </div>
            <Divider className={classes.dividerGradient} />
            <div className={classes.labelContainer}>
                <Typography>{props.label}</Typography>
            </div>
        </div >
    );

}

export default Notification;