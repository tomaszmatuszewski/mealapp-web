import React from 'react';
import {makeStyles, Theme} from '@material-ui/core/styles';
import { Button } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
    button: {
        color: '#6495ed',
        backgroundColor: '#f7f7f7',
        border: '2px solid #6495ed',
        fontWeight: 'bold',

        '&:hover': {
            backgroundColor: 'rgba(243, 150, 154, 0.5)',
        }
    }
}));

const BlueButton = ({disabled, text, buttonType, buttonSize, handleOnClick}: any) => {
    const classes = useStyles();
    return (
        <Button variant="outlined" size={buttonSize} className={classes.button} type={buttonType} disabled={disabled} onClick={handleOnClick} >
        {text}
        </Button>
    );
}

export default BlueButton;