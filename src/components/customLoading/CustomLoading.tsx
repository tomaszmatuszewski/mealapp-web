import React, { useState, useEffect } from 'react';
import icon from '../../img/logo.png';
import { makeStyles, Theme} from '@material-ui/core';
import ViewTitle from '../viewTitle/ViewTitle'

const useStyles = makeStyles((theme: Theme) => ({
    '@keyframes bounce-and-rotate': {
        /* 0%   {top:0px; transform: rotate(0deg);}
        12.5% {top: 50px; transform: rotate(45deg);}
        25%  {top:100px; transform: rotate(90deg);}
        37.5% {top: 150px; transform: rotate(135deg);}
        50%  {top:200px; transform: rotate(180deg);}
        62.5% {top: 150px; transform: rotate(225deg);}
        75%  {top:100px; transform: rotate(270deg);}
        87.5% {top:50px; transform: rotate(315deg);}
        100% {top:0px; transform: rotate(360deg);} */
        '50%': {
            top: '200px',
            '-webkit-transform': 'rotate(180deg)',
        },
        from: {
            '-webkit-transform': 'rotate(0deg)',
            top: '0px',
        },
        to: {
            '-webkit-transform': 'rotate(360deg)',
            top: '0px',
        }
    },
    mainContainer: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        width: '280px',
        margin: 'auto',
    },
    img: {
        height: '100px',
        width: '100px',
        animation: `$bounce-and-rotate 1.5s infinite`,
        position: 'relative',
    },
    iconBox: {
        height: '300px',
        width: '100%',
    }
}));

const CustomLoading = () => {
    const classes = useStyles();
    const [dots, setDots] = useState('');

    useEffect(() => {
        const timer = setTimeout(() => {
            dots.length < 3 ? setDots(dots.concat('.')) : setDots('');
        }, 250);
        return () => clearTimeout(timer);
    }, [dots])

    return (
        <div className={classes.mainContainer}>
        <div className={classes.iconBox}>
            <img className={classes.img} src={icon} alt={'logo'}/>
        </div>
        <ViewTitle title={'Ładowanie' + dots} />
        </div>
    );
}

export default CustomLoading;