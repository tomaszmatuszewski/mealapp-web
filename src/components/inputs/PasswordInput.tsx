import React, {useState} from 'react';
import { createMuiTheme, MuiThemeProvider, Theme, makeStyles } from '@material-ui/core/styles';
import { FormControl, InputLabel, Input, InputAdornment, IconButton } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const useStyles = makeStyles((theme: Theme) => ({
    visibilityIcon: {
        color: '#f3969a',
    },
    label: {
        color: 'black',
        fontWeight: 'bold',
    },
    input: {
        width: '270px',
        fontWeight: 'bold',
    },
}));

const inputTheme = createMuiTheme({
    overrides: {
        MuiInput: {
            underline: {
                '&:after': {
                    borderBottom: `2px solid #6495ed`,
                },
                '&:hover:not($disabled):not($focused):not($error):before': {
                    borderBottom: `2px solid #f3969a`,
                },
            }
        }
    }
})

const PasswordInput = ({labelText, inputId, inputOnChange }: any) => {
    const classes = useStyles();
    const [showPassword, setShowPassword] = useState(false);

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };
    
    return (
        <MuiThemeProvider theme={inputTheme}>
            <FormControl>
                        <InputLabel htmlFor={inputId} className={classes.label}>{labelText}</InputLabel>
                        <Input 
                        type={showPassword ? 'text' : 'password'}
                        id={inputId}
                        className={classes.input}
                        onChange={inputOnChange}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                onClick={handleClickShowPassword}>
                                    {showPassword ? <Visibility className={classes.visibilityIcon}/> : <VisibilityOff className={classes.visibilityIcon}/>}
                                </IconButton>
                            </InputAdornment>
                        }
                        />
                    </FormControl>
        </MuiThemeProvider>
    );
}

export default PasswordInput;