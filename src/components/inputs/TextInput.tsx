import React from 'react';
import { createMuiTheme, MuiThemeProvider, makeStyles, Theme } from '@material-ui/core/styles';
import { FormControl, InputLabel, Input } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
    visibilityIcon: {
        color: '#f3969a',
    },
    label: {
        color: 'black',
        fontWeight: 'bold',
    },
    input: {
        width: '270px',
        fontWeight: 'bold',
    },
}));

const inputTheme = createMuiTheme({
    overrides: {
        MuiInput: {
            underline: {
                '&:after': {
                    borderBottom: `2px solid #6495ed`,
                },
                '&:hover:not($disabled):not($focused):not($error):before': {
                    borderBottom: `2px solid #f3969a`,
                },
            }
        }
    }
})

const TextInput = ({labelText, inputId, inputOnChange }: any) => {
    const classes = useStyles();
    return (
        <MuiThemeProvider theme={inputTheme}>
            <FormControl>
                <InputLabel htmlFor={inputId} className={classes.label}>{labelText}</InputLabel>
                <Input
                    id={inputId}
                    className={classes.input}
                    onChange={inputOnChange}
                />
            </FormControl>
        </MuiThemeProvider>
    );
}

export default TextInput;