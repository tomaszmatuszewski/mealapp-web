import React from 'react';
import { createMuiTheme, MuiThemeProvider, Theme, makeStyles } from '@material-ui/core/styles';
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
    selectItem: {
        color: '#6495ed',
    },
    label: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: '1.5em',
        marginTop: -theme.spacing(1),
    },
    select: {
        backgroundColor: '#ffffff',
        color: '#6495ed',
        border: '2px solid #f3969a',
        width: '270px',
        fontWeight: 'bold',
    },
}));

const inputTheme = createMuiTheme({
    overrides: {
        MuiInput: {
            underline: {
                '&:after': {
                    borderBottom: `2px solid #6495ed`,
                },
                '&:hover:not($disabled):not($focused):not($error):before': {
                    borderBottom: `2px solid #f3969a`,
                },
            }
        }
    }
})

const SelectInput = ({labelId, labelText, disabled, selectValue, inputOnChange, defaultValue, items}: any) => {
    const classes = useStyles();

    return (
        <MuiThemeProvider theme={inputTheme}>
            <FormControl disabled={disabled} >
                <InputLabel id={labelId} className={classes.label}>{labelText}</InputLabel>
                <Select className={classes.select} defaultValue="none" value={selectValue.slug} labelId={labelId} onChange={inputOnChange}>
                    <MenuItem disabled={true} key={'none'} value={'none'} className={classes.selectItem}>
                        {defaultValue}
                    </MenuItem>
                    {!!items && items.map((item : any) => (
                        <MenuItem key={item.slug} value={item} className={classes.selectItem}>
                            {item.name}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </MuiThemeProvider>
    );
}

export default SelectInput;