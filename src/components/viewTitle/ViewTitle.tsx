import React from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        margin: 'auto',
        textAlign: 'center',
        maxWidth: '1100px',
    },
    titleText: {
        color: '#6495ed',
        whiteSpace: 'pre-wrap',
        fontWeight: 'bold',
    },
    row: {
        textAlign: 'center',
    }
}));

const ViewTitle = ({ title }: any) => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <Typography variant="h5" component="h2" className={classes.titleText}>
                {title}
            </Typography>
        </div>
    );
}

export default ViewTitle