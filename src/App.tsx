import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import EatingHouses from './views/EatingHouses';
import MyLocation from './views/MyLocation';
import PageNotFound from './views/PageNotFound';
import Favourites from './views/Favourites';
import Notifications from './views/Notifications';
import Map from './views/Map';
import ShowMore from './views/ShowMore';
import Login from './views/Login';
import Register from './views/Register';
import ForgotPassword from './views/ForgotPassword';
import Account from './views/Account';
import {Provider} from "react-redux";
import store from "./store";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Provider store={store}>
          <Switch>
            <Route exact path="/" component={MyLocation} />
            <Route exact path="/account" component={Account} />
            <Route path="/account/login" component={Login} />
            <Route path="/account/register" component={Register} />
            <Route path="/account/forgotPassword" component={ForgotPassword} />
            <Route path={`/eatingHouses/voivodeship/:voivodeshipName/place/:cityName`} component={EatingHouses} />
            <Route path={`/eatingHouses/:slug/voivodeship/:voivodeshipName/place/:cityName`} component={ShowMore} />
            <Route path="/favourites" component={Favourites} />
            <Route path="/notifications" component={Notifications} />
            <Route path="/map" component={Map} />
            <Route component={PageNotFound} />
          </Switch>
        </Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
