import { INotification } from '../store/reducers/notifications.reducer';
import store from '../store';
import { notificationsActions } from '../store/actions/notifications.actions';

export class NotificationsService {

    addNotification(notification : INotification) {
        store.dispatch(notificationsActions.addNotification(notification));
    }

    setNotificationOn(id: number) {
        store.dispatch(notificationsActions.setNotificationOn(id))
    }

    deleteNotification(id: number) {
        store.dispatch(notificationsActions.deleteNotification(id));
    }

}