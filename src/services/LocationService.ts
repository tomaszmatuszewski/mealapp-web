import http from './utils';
import Connection from './Connection';

const API = {
    URL_VOIVODESHIPS: () => Connection.getVoivodeships(),
    URL_CITIES: (voivodeship: string) => Connection.getCitiesForVoivodeship(voivodeship),
};

export interface IVoivodeshipsProps {
    voivodeships: IVoivodeshipProps[]
}

export interface IVoivodeshipProps {
    name: string,
    slug: string
}

export interface ICitiesProps {
    cities: ICityProps[]
}

export interface ICityProps {
    id: number;
    latitude: number;
    longitude: number;
    zoomWeb: number;
    name: string;
    slug: string;
}

const LocationService = {
    getVoivodeships: async () => {
        try {
            const response = await http.get(API.URL_VOIVODESHIPS());
            const result: IVoivodeshipsProps = {
                voivodeships: response.map((voivodeship: IVoivodeshipProps) => ({
                    name: voivodeship.name,
                    slug: voivodeship.slug
                })),
            };
            return result;
        }
        catch (e) {
            console.log(e);
        }
    },
    getCities: async (props : string) => {
        try {
            const response = await http.get(API.URL_CITIES(props));
            const result: ICitiesProps = {
                cities: response.map((city: ICityProps) => ({
                    id: city.id,
                    latitude: city.latitude,
                    longitude: city.longitude,
                    zoomWeb: city.zoomWeb,
                    name: city.name,
                    slug: city.slug,
                })),
            };
            return result;
        }
        catch (e) {
            console.log(e);
        }
    }
};

export default LocationService;