const http = {
    get: (url: string) => fetch(url)
        .then(resp => resp.json()),
};
//const proxyUrl = 'https://cors-anywhere.herokuapp.com/';

export default http;