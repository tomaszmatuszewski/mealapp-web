import http from './utils';
import Connection from './Connection';

const URL = (voivodeship: string, city: string) => Connection.getEatingHouses(voivodeship, city);

interface IContentProps {
    id: number;
    type: string;
    content: string;
}

export interface IMenuProps {
    id: number;
    date: Date;
    contentList: IContentProps[];
}

export interface IEatingHouseProps {
    id: number;
    name: string;
    menuList: IMenuProps[];
    slug: string;
    eatingHouseTagList: any[];
    logoUrl: string;
    description: string;
    primaryColor: string;
}

export interface IEatingHousesProps {
    eatingHouses: IEatingHouseProps[];
}

const EatingHousesService = {
    getEatingHouses: async (voivodeship: string, city: string) => {
        try {
            const response = await http.get(URL(voivodeship, city));
            const result: IEatingHousesProps = {
                eatingHouses: response.map((eatingHouse: IEatingHouseProps) => ({
                    id: eatingHouse.id,
                    name: eatingHouse.name,
                    menuList: eatingHouse.menuList,
                    slug: eatingHouse.slug,
                    eatingHouseTagList: eatingHouse.eatingHouseTagList,
                    logoUrl: eatingHouse.logoUrl,
                    description: eatingHouse.description,
                    primaryColor: eatingHouse.primaryColor
                })),
            };
            return result;
        }
        catch (e) {
            console.log(e);
        }
    }
}

export default EatingHousesService;