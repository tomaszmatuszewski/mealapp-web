//const URL = "http://u2f38n4o.ddns.net:22222";
export default class Connection {

    static getVoivodeships(){
        return `/voivodeships/slugs`;
    }

    static getCitiesForVoivodeship(voivodeship){
        return `/voivodeships/${voivodeship}/places`;
    }

    static getEatingHouses(voivodeship, city) {
        //return  `http://www.mocky.io/v2/5ea1ce4d310000f7611eee1d`;
        return `/eatingHouses/voivodeship/${voivodeship}/place/${city}`;
    }

    static getShowMore(slug, voivodeship, city) {
        //return `http://www.mocky.io/v2/5e73641c300000d5512e64d0`;
        return `/eatingHouses/${slug}/voivodeship/${voivodeship}/place/${city}`;
    }

    static getFavourites() {
        return `http://www.mocky.io/v2/5e749894300000d431a5f4d3`;
    }

    static getMapMarkers(voivodeship, city){
        //return `http://www.mocky.io/v2/5e7a4d4730000078009309fa`;
        return `/map/eatingHouses/voivodeship/${voivodeship}/place/${city}`;
    }

    static getMap(){
        return process.env.REACT_APP_GOOGLE_API_KEY;
    }

}