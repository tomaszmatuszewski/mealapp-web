import { IShowMoreProps } from '../services/ShowMoreService';
import store from '../store';
import { favouritesActions } from '../store/actions/favourites.actions';


export class FavouritesService {
    addToFavourites(eatingHouse: IShowMoreProps) {
        const favourite: IShowMoreProps = {
            id: eatingHouse.id,
            name: eatingHouse.name,
            menuList: eatingHouse.menuList,
            slug: eatingHouse.slug,
            addressList: eatingHouse.addressList,
            eatingHouseTagList: eatingHouse.eatingHouseTagList,
            logoUrl: eatingHouse.logoUrl,
            description: eatingHouse.description,
            primaryColor: eatingHouse.primaryColor,
            photoUrl: eatingHouse.photoUrl,
        }
        store.dispatch(favouritesActions.addToFavourites(favourite));
    }

    deleteFromFavourites(id: number) {
        store.dispatch(favouritesActions.deleteFromFavourites(id));
    }
}

export default FavouritesService;