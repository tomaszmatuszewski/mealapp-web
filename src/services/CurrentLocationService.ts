import store from '../store';
import { locationActions } from '../store/actions/location.actions';
import {ICityProps, IVoivodeshipProps} from "./LocationService";

export class CurrentLocationService {

    setVoivodeship(voivodeship: IVoivodeshipProps) {
        store.dispatch(locationActions.setVoivodeship(voivodeship));
    }
    setCity(city: ICityProps) {
        store.dispatch(locationActions.setCity(city));
    }

}