import http from './utils';
import Connection from './Connection';

const URL = (slug: string, voivodeship: string, city: string) => Connection.getShowMore(slug, voivodeship, city);

interface IContentProps {
    id: number;
    type: string;
    content: string;
}

export interface IMenuProps {
    id: number;
    date: Date;
    contentList: IContentProps[];
}

interface IOpenHoursProps {
    day: string;
    fromHour: string;
    toHour: string;
}

interface IAddressProps {
    id: number;
    openHoursList: IOpenHoursProps[];
    city: string;
    address: string;
    latitude: number;
    longitude: number;
    phoneNumber: string;
}

export interface IShowMoreProps {
    id: number;
    name: string;
    menuList: IMenuProps[];
    slug: string;
    addressList: IAddressProps[];
    eatingHouseTagList: any[];
    logoUrl: string;
    description: string;
    primaryColor: string;
    photoUrl: string;
}

const ShowMoreService = {
    getShowMore: async (slug: string, voivodeship: string, city: string) => {
        try {
            const response = await http.get(URL(slug, voivodeship, city));
            const result: IShowMoreProps = {
                id: response.id,
                name: response.name,
                menuList: response.menuList,
                slug: response.slug,
                addressList: response.addressList,
                eatingHouseTagList: response.eatingHouseTagList,
                logoUrl: response.logoUrl,
                description: response.description,
                primaryColor: response.primaryColor,
                photoUrl: response.photoUrl,
            }
            return result;
        }

        catch (e) {
            console.log(e);
        }
    }
}

export default ShowMoreService;