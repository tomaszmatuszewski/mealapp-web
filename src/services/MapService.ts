import http from './utils';
import Connection from './Connection';

const URL = (voivodeship: string, city: string) => Connection.getMapMarkers(voivodeship, city);

export interface IMapMarkersProps {
    mapMarkers: IMapMarkerProps[];
}

export interface IMapMarkerProps {
    id: number;
    name: string;
    slug: string;
    addressList: IAddressProps[];
}

export interface IAddressProps {
    id: number;
    city: string;
    address: string;
    latitude: number;
    longitude: number;
    phoneNumber: string;
}

const MapService = {
    getMapMarkers: async (voivodeship: string, city: string) => {
        try {
            const response = await http.get(URL(voivodeship, city));
            const result: IMapMarkersProps = {
                mapMarkers: response.map((mapMarker: IMapMarkerProps) => ({
                    id: mapMarker.id,
                    name: mapMarker.name,
                    slug: mapMarker.slug,
                    addressList: mapMarker.addressList
                    // addressList: mapMarker.addressList.map((address: IAddressProps) => ({
                    //     id: address.id,
                    //     city: address.city,
                    //     address: address.address,
                    //     latitude: address.latitude,
                    //     longitude: address.longitude,
                    //     phoneNumber: address.phoneNumber,
                    // }))
                })),
            };
            return result;
        }
        catch(e) {
            console.log(e)
        }
    }
};

export default MapService;