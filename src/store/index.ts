import { createStore } from 'redux';
import {composeWithDevTools} from "redux-devtools-extension";
import {IStoreState, reducers, StoreActions} from "./store";
import {loadState, saveState} from "./localStorage";
import {throttle} from 'lodash'

const persistedState = loadState();
const store = createStore<IStoreState, StoreActions, any, any>(reducers, persistedState, composeWithDevTools());

store.subscribe(throttle(() => {
    saveState(store.getState());
}, 1000));

export default store;