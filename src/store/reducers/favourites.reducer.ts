import { Reducer } from 'redux';
import { Actions, FavouritesActions } from '../actions/favourites.actions'
import { IShowMoreProps } from '../../services/ShowMoreService';

export interface IFavouriteEatingHousesStoreState {
    favourites: IShowMoreProps[],
}

export const favouritesInitialState: IFavouriteEatingHousesStoreState = {
    favourites: []
};

export const favouritesStoreReducer: Reducer<IFavouriteEatingHousesStoreState, Actions> = (state: IFavouriteEatingHousesStoreState = favouritesInitialState, actions: Actions) => {
    switch (actions.type) {

        case FavouritesActions.ADD_TO_FAVOURITES:
            return {
                ...state,
                favourites: [...state.favourites, actions.payload.eatingHouse]
            };

        case FavouritesActions.DELETE_FROM_FAVOURITES:
            return {
                ...state,
                favourites: state.favourites.filter(el => el.id !== actions.payload.id)
            };

        default:
            return state;
    }
}