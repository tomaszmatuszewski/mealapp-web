import { Reducer } from 'redux'
import { Actions, NotificationsStoreActions } from '../actions/notifications.actions'

export interface INotification {
    id: number;
    label: string;
    isOn: boolean;
}

export interface INotificationsStoreState {
    notifications: INotification[];
}

export const notificationsInitialState: INotificationsStoreState = {
    notifications: []
};

export const notificationsStoreReducer: Reducer<INotificationsStoreState, Actions> = (state: INotificationsStoreState = notificationsInitialState, actions: Actions) => {
    switch (actions.type) {
        case NotificationsStoreActions.ADD_NOTIFICATION:
            return {
                ...state,
                notifications: [...state.notifications, actions.payload.notification]
            };
        case NotificationsStoreActions.SET_NOTIFICATION_ON:
            const notificationFound = state.notifications.find(el => el.id === actions.payload.id);
            if (notificationFound) {
                notificationFound.isOn = !notificationFound.isOn;
                return {
                    ...state,
                    notifications: [...state.notifications],
                };
            }
            return state;

            case NotificationsStoreActions.DELETE_NOTIFICATION:
                return {
                    ...state,
                    notifications: state.notifications.filter(el => el.id !== actions.payload.id)
                }

        default:
            return state;
    }
}