import {Reducer} from "redux";
import {Actions, LocationStoreActions} from "../actions/location.actions";

export interface ILocationStoreState {
    voivodeshipName: string;
    voivodeshipSlug: string;
    cityName: string;
    citySlug: string;
    latitude: number;
    longitude: number;
    zoom: number;
    cityId: number;
}

export const locationInitialState: ILocationStoreState = {
    voivodeshipName: 'none',
    voivodeshipSlug: 'none',
    cityName: 'none',
    citySlug: 'none',
    latitude: -1,
    longitude: -1,
    zoom: -1,
    cityId: -1,
};

export const locationStoreReducer: Reducer<ILocationStoreState, Actions>=(state: ILocationStoreState=locationInitialState, actions: Actions) => {
    switch (actions.type) {
        case LocationStoreActions.SET_VOIVODESHIP:
            return{
                //...state,
                voivodeshipName: actions.payload.voivodeship.name,
                voivodeshipSlug: actions.payload.voivodeship.slug,
                cityName: 'none',
                citySlug: 'none',
                latitude: -1,
                longitude: -1,
                zoom: -1,
                cityId: -1,
            };
        case LocationStoreActions.SET_CITY:
            return {
                ...state,
                cityName: actions.payload.city.name,
                citySlug: actions.payload.city.slug,
                latitude: actions.payload.city.latitude,
                longitude: actions.payload.city.longitude,
                zoom: actions.payload.city.zoomWeb,
                cityId: actions.payload.city.id,
            };

        default:
            return state;
    }
};
