import { INotification } from '../reducers/notifications.reducer';

export enum NotificationsStoreActions {
    ADD_NOTIFICATION = 'ADD_NOTIFICATION',
    SET_NOTIFICATION_ON = 'SET_NOTIFICATION_ON',
    DELETE_NOTIFICATION = 'DELETE_NOTIFICATION',
}

export interface IAddNotification {
    type: NotificationsStoreActions.ADD_NOTIFICATION,
    payload: {
        notification: INotification
    }
}

export interface ISetNotificationOn {
    type: NotificationsStoreActions.SET_NOTIFICATION_ON,
    payload: {
        id: number
    }
}

export interface IDeleteNotification {
    type: NotificationsStoreActions.DELETE_NOTIFICATION,
    payload: {
        id: number
    }
}

export const notificationsActions = {
    addNotification: (notification: INotification) => ({
        type: NotificationsStoreActions.ADD_NOTIFICATION,
        payload: {
            notification
        }
    }),
    setNotificationOn: (id: number) => ({
        type: NotificationsStoreActions.SET_NOTIFICATION_ON,
        payload: {
            id
        }
    }),
    deleteNotification: (id: number) => ({
        type: NotificationsStoreActions.DELETE_NOTIFICATION,
        payload: {
            id
        }
    })
}

export type Actions = IAddNotification & ISetNotificationOn & IDeleteNotification;