import {ICityProps, IVoivodeshipProps} from "../../services/LocationService";

export enum LocationStoreActions {
    SET_VOIVODESHIP = 'SET_VOIVODESHIP',
    SET_CITY = 'SET_CITY',
}

export interface ISetVoivodeship {
    type: LocationStoreActions.SET_VOIVODESHIP,
    payload:{
        voivodeship: IVoivodeshipProps
    }
}

export interface ISetCity {
    type: LocationStoreActions.SET_CITY,
    payload:{
        city: ICityProps
    }
}

export const locationActions = {
    setVoivodeship: (voivodeship: IVoivodeshipProps) => ({
        type: LocationStoreActions.SET_VOIVODESHIP,
        payload:{
            voivodeship
        }
    }),

    setCity: (city: ICityProps) => ({
        type: LocationStoreActions.SET_CITY,
        payload:{
            city
        }
    }),
};

export type Actions = ISetVoivodeship & ISetCity;