import { IShowMoreProps } from '../../services/ShowMoreService'

export enum FavouritesActions {
    ADD_TO_FAVOURITES = 'ADD_TO_FAVOURITES',
    DELETE_FROM_FAVOURITES = 'DELETE_FROM_FAVOURITES',
}

export interface IAddToFavourites {
    type: FavouritesActions.ADD_TO_FAVOURITES,
    payload: {
        eatingHouse: IShowMoreProps,
    }
}

export interface IDeleteFromFavourites {
    type: FavouritesActions.DELETE_FROM_FAVOURITES,
    payload: {
        id: number,
    }
}

export const favouritesActions = {
    addToFavourites: (eatingHouse: IShowMoreProps) => ({
        type: FavouritesActions.ADD_TO_FAVOURITES,
        payload: {
            eatingHouse
        }
    }),
    deleteFromFavourites: (id:number) => ({
        type: FavouritesActions.DELETE_FROM_FAVOURITES,
        payload: {
            id
        }
    })
};

export type Actions = IAddToFavourites & IDeleteFromFavourites;