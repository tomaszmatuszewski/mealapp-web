import { IStoreState } from '../store';
import { createSelector } from 'reselect';

export const locationSelector = createSelector(
    (state: IStoreState) => state.location,
    location => location
);