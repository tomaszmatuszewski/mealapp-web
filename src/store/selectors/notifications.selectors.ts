import { IStoreState } from '../../store/store';
import { createSelector } from 'reselect';

export const notificationsSelector = createSelector(
    (state: IStoreState) => state.notifications,
    notifications => notifications
);