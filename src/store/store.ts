import { combineReducers } from "redux";
import * as LocationActions from './actions/location.actions'
import {ILocationStoreState, locationInitialState, locationStoreReducer} from "./reducers/location.reducer";
import { IFavouriteEatingHousesStoreState, favouritesInitialState, favouritesStoreReducer } from "./reducers/favourites.reducer";
import { FavouritesActions } from "./actions/favourites.actions";
import { INotificationsStoreState, notificationsInitialState, notificationsStoreReducer } from "./reducers/notifications.reducer";

export type StoreActions = LocationActions.Actions & FavouritesActions;

export interface IStoreState {
    location: ILocationStoreState,
    favourites: IFavouriteEatingHousesStoreState
    notifications: INotificationsStoreState
}

export const initialStoreState: IStoreState = {
    location: locationInitialState,
    favourites: favouritesInitialState,
    notifications: notificationsInitialState
};

export const reducers = combineReducers<IStoreState>({
    location: locationStoreReducer,
    favourites: favouritesStoreReducer,
    notifications: notificationsStoreReducer
});